function MediumEditor(elements, options) {
    'use strict';
    return this.init(elements, options);
}

if (typeof module === 'object') {
    module.exports = MediumEditor;
}

(function (window, document) {
    'use strict';

    function extend(b, a) {
        var prop;
        if (b === undefined) {
            return a;
        }
        for (prop in a) {
            if (a.hasOwnProperty(prop) && b.hasOwnProperty(prop) === false) {
                b[prop] = a[prop];
            }
        }
        return b;
    }

    // http://stackoverflow.com/questions/5605401/insert-link-in-contenteditable-element
    // by Tim Down
    function saveSelection() {
        var i,
            len,
            ranges,
            sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            ranges = [];
            for (i = 0, len = sel.rangeCount; i < len; i += 1) {
                ranges.push(sel.getRangeAt(i));
            }
            return ranges;
        }
        return null;
    }

    function restoreSelection(savedSel) {
        var i,
            len,
            sel = window.getSelection();
        if (savedSel) {
            sel.removeAllRanges();
            for (i = 0, len = savedSel.length; i < len; i += 1) {
                sel.addRange(savedSel[i]);
            }
        }
    }

    // http://stackoverflow.com/questions/1197401/how-can-i-get-the-element-the-caret-is-in-with-javascript-when-using-contentedi
    // by You
    function getSelectionStart() {
        var node = document.getSelection().anchorNode,
            startNode = (node && node.nodeType === 3 ? node.parentNode : node);
        return startNode;
    }

    // http://stackoverflow.com/questions/4176923/html-of-selected-text
    // by Tim Down
    function getSelectionHtml() {
        var i,
            html = '',
            sel,
            len,
            container;
        if (window.getSelection !== undefined) {
            sel = window.getSelection();
            if (sel.rangeCount) {
                container = document.createElement('div');
                for (i = 0, len = sel.rangeCount; i < len; i += 1) {
                    container.appendChild(sel.getRangeAt(i).cloneContents());
                }
                html = container.innerHTML;
            }
        } else if (document.selection !== undefined) {
            if (document.selection.type === 'Text') {
                html = document.selection.createRange().htmlText;
            }
        }
        return html;
    }

    // https://github.com/jashkenas/underscore
    function isElement(obj) {
        return !!(obj && obj.nodeType === 1);
    }

    MediumEditor.prototype = {
        defaults: {
            allowMultiParagraphSelection: true,
            anchorInputPlaceholder: 'Paste or type a link',
            anchorPreviewHideDelay: 500,
            buttons: ['bold', 'italic', 'underline', 'anchor', 'header1', 'header2', 'quote'],
            buttonLabels: false,
            checkLinkFormat: false,
            cleanPastedHTML: false,
            delay: 0,
            diffLeft: 0,
            diffTop: -10,
            disableReturn: false,
            disableDoubleReturn: false,
            disableToolbar: false,
            disableEditing: false,
            elementsContainer: false,
            firstHeader: 'h3',
            forcePlainText: true,
            placeholder: 'Type your text',
            secondHeader: 'h4',
            targetBlank: false,
            extensions: {},
            activeButtonClass: 'medium-editor-button-active',
            firstButtonClass: 'medium-editor-button-first',
            lastButtonClass: 'medium-editor-button-last'
        },

        // http://stackoverflow.com/questions/17907445/how-to-detect-ie11#comment30165888_17907562
        // by rg89
        isIE: ((navigator.appName === 'Microsoft Internet Explorer') || ((navigator.appName === 'Netscape') && (new RegExp('Trident/.*rv:([0-9]{1,}[.0-9]{0,})').exec(navigator.userAgent) !== null))),

        init: function (elements, options) {
            this.setElementSelection(elements);
            if (this.elements.length === 0) {
                return;
            }
            this.parentElements = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre'];
            this.id = document.querySelectorAll('.medium-editor-toolbar').length + 1;
            this.options = extend(options, this.defaults);
            return this.setup();
        },

        setup: function () {
            this.isActive = true;
            this.initElements()
                .bindSelect()
                .bindPaste()
                .setPlaceholders()
                .bindWindowActions();
        },

        initElements: function () {
            this.updateElementList();
            var i,
                addToolbar = false;
            for (i = 0; i < this.elements.length; i += 1) {
                if (!this.options.disableEditing && !this.elements[i].getAttribute('data-disable-editing')) {
                    this.elements[i].setAttribute('contentEditable', true);
                }
                if (!this.elements[i].getAttribute('data-placeholder')) {
                    this.elements[i].setAttribute('data-placeholder', this.options.placeholder);
                }
                this.elements[i].setAttribute('data-medium-element', true);
                this.bindParagraphCreation(i).bindReturn(i).bindTab(i);
                if (!this.options.disableToolbar && !this.elements[i].getAttribute('data-disable-toolbar')) {
                    addToolbar = true;
                }
            }
            // Init toolbar
            if (addToolbar) {
                if (!this.options.elementsContainer) {
                    this.options.elementsContainer = document.body;
                }
                this.initToolbar()
                    .bindButtons()
                    .bindAnchorForm()
                    .bindAnchorPreview();
            }
            return this;
        },

        setElementSelection: function (selector) {
            this.elementSelection = selector;
            this.updateElementList();
        },

        updateElementList: function () {
            this.elements = typeof this.elementSelection === 'string' ? document.querySelectorAll(this.elementSelection) : this.elementSelection;
            if (this.elements.nodeType === 1) {
                this.elements = [this.elements];
            }
        },

        serialize: function () {
            var i,
                elementid,
                content = {};
            for (i = 0; i < this.elements.length; i += 1) {
                elementid = (this.elements[i].id !== '') ? this.elements[i].id : 'element-' + i;
                content[elementid] = {
                    value: this.elements[i].innerHTML.trim()
                };
            }
            return content;
        },

        /**
         * Helper function to call a method with a number of parameters on all registered extensions.
         * The function assures that the function exists before calling.
         *
         * @param {string} funcName name of the function to call
         * @param [args] arguments passed into funcName
         */
        callExtensions: function (funcName) {
            if (arguments.length < 1) {
                return;
            }

            var args = Array.prototype.slice.call(arguments, 1),
                ext,
                name;

            for (name in this.options.extensions) {
                if (this.options.extensions.hasOwnProperty(name)) {
                    ext = this.options.extensions[name];
                    if (ext[funcName] !== undefined) {
                        ext[funcName].apply(ext, args);
                    }
                }
            }
        },

        bindParagraphCreation: function (index) {
            var self = this;
            this.elements[index].addEventListener('keypress', function (e) {
                var node = getSelectionStart(),
                    tagName;
                if (e.which === 32) {
                    tagName = node.tagName.toLowerCase();
                    if (tagName === 'a') {
                        document.execCommand('unlink', false, null);
                    }
                }
            });

            this.elements[index].addEventListener('keyup', function (e) {
                var node = getSelectionStart(),
                    tagName;
                if (node && node.getAttribute('data-medium-element') && node.children.length === 0 && !(self.options.disableReturn || node.getAttribute('data-disable-return'))) {
                    document.execCommand('formatBlock', false, 'p');
                }
                if (e.which === 13) {
                    node = getSelectionStart();
                    tagName = node.tagName.toLowerCase();
                    if (!(self.options.disableReturn || this.getAttribute('data-disable-return')) &&
                        tagName !== 'li' && !self.isListItemChild(node)) {
                        if (!e.shiftKey) {
                            document.execCommand('formatBlock', false, 'p');
                        }
                        if (tagName === 'a') {
                            document.execCommand('unlink', false, null);
                        }
                    }
                }
            });
            return this;
        },

        isListItemChild: function (node) {
            var parentNode = node.parentNode,
                tagName = parentNode.tagName.toLowerCase();
            while (this.parentElements.indexOf(tagName) === -1 && tagName !== 'div') {
                if (tagName === 'li') {
                    return true;
                }
                parentNode = parentNode.parentNode;
                if (parentNode && parentNode.tagName) {
                    tagName = parentNode.tagName.toLowerCase();
                } else {
                    return false;
                }
            }
            return false;
        },

        bindReturn: function (index) {
            var self = this;
            this.elements[index].addEventListener('keypress', function (e) {
                if (e.which === 13) {
                    if (self.options.disableReturn || this.getAttribute('data-disable-return')) {
                        e.preventDefault();
                    } else if (self.options.disableDoubleReturn || this.getAttribute('data-disable-double-return')) {
                        var node = getSelectionStart();
                        if (node && node.innerText === '\n') {
                            e.preventDefault();
                        }
                    }
                }
            });
            return this;
        },

        bindTab: function (index) {
            this.elements[index].addEventListener('keydown', function (e) {
                if (e.which === 9) {
                    // Override tab only for pre nodes
                    var tag = getSelectionStart().tagName.toLowerCase();
                    if (tag === 'pre') {
                        e.preventDefault();
                        document.execCommand('insertHtml', null, '    ');
                    }
                }
            });
            return this;
        },

        buttonTemplate: function (btnType) {
            var buttonLabels = this.getButtonLabels(this.options.buttonLabels),
                buttonTemplates = {
                    'bold': '<button class="medium-editor-action medium-editor-action-bold" data-action="bold" data-element="b">' + buttonLabels.bold + '</button>',
                    'italic': '<button class="medium-editor-action medium-editor-action-italic" data-action="italic" data-element="i">' + buttonLabels.italic + '</button>',
                    'underline': '<button class="medium-editor-action medium-editor-action-underline" data-action="underline" data-element="u">' + buttonLabels.underline + '</button>',
                    'strikethrough': '<button class="medium-editor-action medium-editor-action-strikethrough" data-action="strikethrough" data-element="strike"><strike>A</strike></button>',
                    'superscript': '<button class="medium-editor-action medium-editor-action-superscript" data-action="superscript" data-element="sup">' + buttonLabels.superscript + '</button>',
                    'subscript': '<button class="medium-editor-action medium-editor-action-subscript" data-action="subscript" data-element="sub">' + buttonLabels.subscript + '</button>',
                    'anchor': '<button class="medium-editor-action medium-editor-action-anchor" data-action="anchor" data-element="a">' + buttonLabels.anchor + '</button>',
                    'image': '<button class="medium-editor-action medium-editor-action-image" data-action="image" data-element="img">' + buttonLabels.image + '</button>',
                    'header1': '<button class="medium-editor-action medium-editor-action-header1" data-action="append-' + this.options.firstHeader + '" data-element="' + this.options.firstHeader + '">' + buttonLabels.header1 + '</button>',
                    'header2': '<button class="medium-editor-action medium-editor-action-header2" data-action="append-' + this.options.secondHeader + '" data-element="' + this.options.secondHeader + '">' + buttonLabels.header2 + '</button>',
                    'quote': '<button class="medium-editor-action medium-editor-action-quote" data-action="append-blockquote" data-element="blockquote">' + buttonLabels.quote + '</button>',
                    'orderedlist': '<button class="medium-editor-action medium-editor-action-orderedlist" data-action="insertorderedlist" data-element="ol">' + buttonLabels.orderedlist + '</button>',
                    'unorderedlist': '<button class="medium-editor-action medium-editor-action-unorderedlist" data-action="insertunorderedlist" data-element="ul">' + buttonLabels.unorderedlist + '</button>',
                    'pre': '<button class="medium-editor-action medium-editor-action-pre" data-action="append-pre" data-element="pre">' + buttonLabels.pre + '</button>',
                    'indent': '<button class="medium-editor-action medium-editor-action-indent" data-action="indent" data-element="ul">' + buttonLabels.indent + '</button>',
                    'outdent': '<button class="medium-editor-action medium-editor-action-outdent" data-action="outdent" data-element="ul">' + buttonLabels.outdent + '</button>'
                };
            return buttonTemplates[btnType] || false;
        },

        // TODO: break method
        getButtonLabels: function (buttonLabelType) {
            var customButtonLabels,
                attrname,
                buttonLabels = {
                    'bold': '<b>B</b>',
                    'italic': '<b><i>I</i></b>',
                    'underline': '<b><u>U</u></b>',
                    'superscript': '<b>x<sup>1</sup></b>',
                    'subscript': '<b>x<sub>1</sub></b>',
                    'anchor': '<b>#</b>',
                    'image': '<b>image</b>',
                    'header1': '<b>H1</b>',
                    'header2': '<b>H2</b>',
                    'quote': '<b>&ldquo;</b>',
                    'orderedlist': '<b>1.</b>',
                    'unorderedlist': '<b>&bull;</b>',
                    'pre': '<b>0101</b>',
                    'indent': '<b>&rarr;</b>',
                    'outdent': '<b>&larr;</b>'
                };
            if (buttonLabelType === 'fontawesome') {
                customButtonLabels = {
                    'bold': '<i class="fa fa-bold"></i>',
                    'italic': '<i class="fa fa-italic"></i>',
                    'underline': '<i class="fa fa-underline"></i>',
                    'superscript': '<i class="fa fa-superscript"></i>',
                    'subscript': '<i class="fa fa-subscript"></i>',
                    'anchor': '<i class="fa fa-link"></i>',
                    'image': '<i class="fa fa-picture-o"></i>',
                    'quote': '<i class="fa fa-quote-right"></i>',
                    'orderedlist': '<i class="fa fa-list-ol"></i>',
                    'unorderedlist': '<i class="fa fa-list-ul"></i>',
                    'pre': '<i class="fa fa-code fa-lg"></i>',
                    'indent': '<i class="fa fa-indent"></i>',
                    'outdent': '<i class="fa fa-outdent"></i>'
                };
            } else if (typeof buttonLabelType === 'object') {
                customButtonLabels = buttonLabelType;
            }
            if (typeof customButtonLabels === 'object') {
                for (attrname in customButtonLabels) {
                    if (customButtonLabels.hasOwnProperty(attrname)) {
                        buttonLabels[attrname] = customButtonLabels[attrname];
                    }
                }
            }
            return buttonLabels;
        },

        initToolbar: function () {
            if (this.toolbar) {
                return this;
            }
            this.toolbar = this.createToolbar();
            this.keepToolbarAlive = false;
            this.anchorForm = this.toolbar.querySelector('.medium-editor-toolbar-form-anchor');
            this.anchorInput = this.anchorForm.querySelector('input');
            this.toolbarActions = this.toolbar.querySelector('.medium-editor-toolbar-actions');
            this.anchorPreview = this.createAnchorPreview();

            return this;
        },

        createToolbar: function () {
            var toolbar = document.createElement('div');
            toolbar.id = 'medium-editor-toolbar-' + this.id;
            toolbar.className = 'medium-editor-toolbar';
            toolbar.appendChild(this.toolbarButtons());
            toolbar.appendChild(this.toolbarFormAnchor());
            this.options.elementsContainer.appendChild(toolbar);
            return toolbar;
        },

        //TODO: actionTemplate
        toolbarButtons: function () {
            var btns = this.options.buttons,
                ul = document.createElement('ul'),
                li,
                i,
                btn,
                ext;

            ul.id = 'medium-editor-toolbar-actions';
            ul.className = 'medium-editor-toolbar-actions clearfix';

            for (i = 0; i < btns.length; i += 1) {
                if (this.options.extensions.hasOwnProperty(btns[i])) {
                    ext = this.options.extensions[btns[i]];
                    btn = ext.getButton !== undefined ? ext.getButton() : null;
                } else {
                    btn = this.buttonTemplate(btns[i]);
                }

                if (btn) {
                    li = document.createElement('li');
                    if (isElement(btn)) {
                        li.appendChild(btn);
                    } else {
                        li.innerHTML = btn;
                    }
                    ul.appendChild(li);
                }
            }

            return ul;
        },

        toolbarFormAnchor: function () {
            var anchor = document.createElement('div'),
                input = document.createElement('input'),
                a = document.createElement('a');

            a.setAttribute('href', '#');
            a.innerHTML = '&times;';

            input.setAttribute('type', 'text');
            input.setAttribute('placeholder', this.options.anchorInputPlaceholder);

            anchor.className = 'medium-editor-toolbar-form-anchor';
            anchor.id = 'medium-editor-toolbar-form-anchor';
            anchor.appendChild(input);
            anchor.appendChild(a);

            return anchor;
        },

        bindSelect: function () {
            var self = this,
                timer = '',
                i;

            this.checkSelectionWrapper = function (e) {

                // Do not close the toolbar when bluring the editable area and clicking into the anchor form
                if (e && self.clickingIntoArchorForm(e)) {
                    return false;
                }

                clearTimeout(timer);
                timer = setTimeout(function () {
                    self.checkSelection();
                }, self.options.delay);
            };

            document.documentElement.addEventListener('mouseup', this.checkSelectionWrapper);

            for (i = 0; i < this.elements.length; i += 1) {
                this.elements[i].addEventListener('keyup', this.checkSelectionWrapper);
                this.elements[i].addEventListener('blur', this.checkSelectionWrapper);
            }
            return this;
        },

        checkSelection: function () {
            var newSelection,
                selectionElement;

            if (this.keepToolbarAlive !== true && !this.options.disableToolbar) {
                newSelection = window.getSelection();
                if (newSelection.toString().trim() === '' ||
                    (this.options.allowMultiParagraphSelection === false && this.hasMultiParagraphs())) {
                    this.hideToolbarActions();
                } else {
                    selectionElement = this.getSelectionElement();
                    if (!selectionElement || selectionElement.getAttribute('data-disable-toolbar')) {
                        this.hideToolbarActions();
                    } else {
                        this.checkSelectionElement(newSelection, selectionElement);
                    }
                }
            }
            return this;
        },

        clickingIntoArchorForm: function (e) {
            var self = this;
            if (e.type && e.type.toLowerCase() === 'blur' && e.relatedTarget && e.relatedTarget === self.anchorInput) {
                return true;
            }
            return false;
        },

        hasMultiParagraphs: function () {
            var selectionHtml = getSelectionHtml().replace(/<[\S]+><\/[\S]+>/gim, ''),
                hasMultiParagraphs = selectionHtml.match(/<(p|h[0-6]|blockquote)>([\s\S]*?)<\/(p|h[0-6]|blockquote)>/g);

            return (hasMultiParagraphs ? hasMultiParagraphs.length : 0);
        },

        checkSelectionElement: function (newSelection, selectionElement) {
            var i;
            this.selection = newSelection;
            this.selectionRange = this.selection.getRangeAt(0);
            for (i = 0; i < this.elements.length; i += 1) {
                if (this.elements[i] === selectionElement) {
                    this.setToolbarButtonStates()
                        .setToolbarPosition()
                        .showToolbarActions();
                    return;
                }
            }
            this.hideToolbarActions();
        },

        getSelectionElement: function () {
            var selection = window.getSelection(),
                range, current, parent,
                result,
                getMediumElement = function (e) {
                    var localParent = e;
                    try {
                        while (!localParent.getAttribute('data-medium-element')) {
                            localParent = localParent.parentNode;
                        }
                    } catch (errb) {
                        return false;
                    }
                    return localParent;
                };
            // First try on current node
            try {
                range = selection.getRangeAt(0);
                current = range.commonAncestorContainer;
                parent = current.parentNode;

                if (current.getAttribute('data-medium-element')) {
                    result = current;
                } else {
                    result = getMediumElement(parent);
                }
                // If not search in the parent nodes.
            } catch (err) {
                result = getMediumElement(parent);
            }
            return result;
        },

        setToolbarPosition: function () {
            var buttonHeight = 50,
                selection = window.getSelection(),
                range = selection.getRangeAt(0),
                boundary = range.getBoundingClientRect(),
                defaultLeft = (this.options.diffLeft) - (this.toolbar.offsetWidth / 2),
                middleBoundary = (boundary.left + boundary.right) / 2,
                halfOffsetWidth = this.toolbar.offsetWidth / 2;
            if (boundary.top < buttonHeight) {
                this.toolbar.classList.add('medium-toolbar-arrow-over');
                this.toolbar.classList.remove('medium-toolbar-arrow-under');
                this.toolbar.style.top = buttonHeight + boundary.bottom - this.options.diffTop + window.pageYOffset - this.toolbar.offsetHeight + 'px';
            } else {
                this.toolbar.classList.add('medium-toolbar-arrow-under');
                this.toolbar.classList.remove('medium-toolbar-arrow-over');
                this.toolbar.style.top = boundary.top + this.options.diffTop + window.pageYOffset - this.toolbar.offsetHeight + 'px';
            }
            if (middleBoundary < halfOffsetWidth) {
                this.toolbar.style.left = defaultLeft + halfOffsetWidth + 'px';
            } else if ((window.innerWidth - middleBoundary) < halfOffsetWidth) {
                this.toolbar.style.left = window.innerWidth + defaultLeft - halfOffsetWidth + 'px';
            } else {
                this.toolbar.style.left = defaultLeft + middleBoundary + 'px';
            }

            this.hideAnchorPreview();

            return this;
        },

        setToolbarButtonStates: function () {
            var buttons = this.toolbarActions.querySelectorAll('button'),
                i;
            for (i = 0; i < buttons.length; i += 1) {
                buttons[i].classList.remove(this.options.activeButtonClass);
            }
            this.checkActiveButtons();
            return this;
        },

        checkActiveButtons: function () {
            var elements = Array.prototype.slice.call(this.elements),
                parentNode = this.getSelectedParentElement();
            while (parentNode.tagName !== undefined && this.parentElements.indexOf(parentNode.tagName.toLowerCase) === -1) {
                this.activateButton(parentNode.tagName.toLowerCase());
                this.callExtensions('checkState', parentNode);

                // we can abort the search upwards if we leave the contentEditable element
                if (elements.indexOf(parentNode) !== -1) {
                    break;
                }
                parentNode = parentNode.parentNode;
            }
        },

        activateButton: function (tag) {
            var el = this.toolbar.querySelector('[data-element="' + tag + '"]');
            if (el !== null && el.className.indexOf(this.options.activeButtonClass) === -1) {
                el.className += ' ' + this.options.activeButtonClass;
            }
        },

        bindButtons: function () {
            var buttons = this.toolbar.querySelectorAll('button'),
                i,
                self = this,
                triggerAction = function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (self.selection === undefined) {
                        self.checkSelection();
                    }
                    if (this.className.indexOf(self.options.activeButtonClass) > -1) {
                        this.classList.remove(self.options.activeButtonClass);
                    } else {
                        this.className += ' ' + self.options.activeButtonClass;
                    }
                    if (this.hasAttribute('data-action')) {
                        self.execAction(this.getAttribute('data-action'), e);
                    }
                };
            for (i = 0; i < buttons.length; i += 1) {
                buttons[i].addEventListener('click', triggerAction);
            }
            this.setFirstAndLastItems(buttons);
            return this;
        },

        setFirstAndLastItems: function (buttons) {
            if (buttons.length > 0) {
                buttons[0].className += ' ' + this.options.firstButtonClass;
                buttons[buttons.length - 1].className += ' ' + this.options.lastButtonClass;
            }
            return this;
        },

        execAction: function (action, e) {
            if (action.indexOf('append-') > -1) {
                this.execFormatBlock(action.replace('append-', ''));
                this.setToolbarPosition();
                this.setToolbarButtonStates();
            } else if (action === 'anchor') {
                this.triggerAnchorAction(e);
            } else if (action === 'image') {
                document.execCommand('insertImage', false, window.getSelection());
            } else {
                document.execCommand(action, false, null);
                this.setToolbarPosition();
            }
        },

        // http://stackoverflow.com/questions/15867542/range-object-get-selection-parent-node-chrome-vs-firefox
        rangeSelectsSingleNode: function (range) {
            var startNode = range.startContainer;
            return startNode === range.endContainer &&
                startNode.hasChildNodes() &&
                range.endOffset === range.startOffset + 1;
        },

        getSelectedParentElement: function () {
            var selectedParentElement = null,
                range = this.selectionRange;
            if (this.rangeSelectsSingleNode(range)) {
                selectedParentElement = range.startContainer.childNodes[range.startOffset];
            } else if (range.startContainer.nodeType === 3) {
                selectedParentElement = range.startContainer.parentNode;
            } else {
                selectedParentElement = range.startContainer;
            }
            return selectedParentElement;
        },

        triggerAnchorAction: function () {
            var selectedParentElement = this.getSelectedParentElement();
            if (selectedParentElement.tagName &&
                    selectedParentElement.tagName.toLowerCase() === 'a') {
                document.execCommand('unlink', false, null);
            } else {
                if (this.anchorForm.style.display === 'block') {
                    this.showToolbarActions();
                } else {
                    this.showAnchorForm();
                }
            }
            return this;
        },

        execFormatBlock: function (el) {
            var selectionData = this.getSelectionData(this.selection.anchorNode);
            // FF handles blockquote differently on formatBlock
            // allowing nesting, we need to use outdent
            // https://developer.mozilla.org/en-US/docs/Rich-Text_Editing_in_Mozilla
            if (el === 'blockquote' && selectionData.el &&
                selectionData.el.parentNode.tagName.toLowerCase() === 'blockquote') {
                return document.execCommand('outdent', false, null);
            }
            if (selectionData.tagName === el) {
                el = 'p';
            }
            // When IE we need to add <> to heading elements and
            //  blockquote needs to be called as indent
            // http://stackoverflow.com/questions/10741831/execcommand-formatblock-headings-in-ie
            // http://stackoverflow.com/questions/1816223/rich-text-editor-with-blockquote-function/1821777#1821777
            if (this.isIE) {
                if (el === 'blockquote') {
                    return document.execCommand('indent', false, el);
                }
                el = '<' + el + '>';
            }
            return document.execCommand('formatBlock', false, el);
        },

        getSelectionData: function (el) {
            var tagName;

            if (el && el.tagName) {
                tagName = el.tagName.toLowerCase();
            }

            while (el && this.parentElements.indexOf(tagName) === -1) {
                el = el.parentNode;
                if (el && el.tagName) {
                    tagName = el.tagName.toLowerCase();
                }
            }

            return {
                el: el,
                tagName: tagName
            };
        },

        getFirstChild: function (el) {
            var firstChild = el.firstChild;
            while (firstChild !== null && firstChild.nodeType !== 1) {
                firstChild = firstChild.nextSibling;
            }
            return firstChild;
        },

        hideToolbarActions: function () {
            this.keepToolbarAlive = false;
            if (this.toolbar !== undefined) {
                this.toolbar.classList.remove('medium-editor-toolbar-active');
            }
        },

        showToolbarActions: function () {
            var self = this,
                timer;
            this.anchorForm.style.display = 'none';
            this.toolbarActions.style.display = 'block';
            this.keepToolbarAlive = false;
            clearTimeout(timer);
            timer = setTimeout(function () {
                if (self.toolbar && !self.toolbar.classList.contains('medium-editor-toolbar-active')) {
                    self.toolbar.classList.add('medium-editor-toolbar-active');
                }
            }, 100);
        },

        saveSelection: function() {
            this.savedSelection = saveSelection();
        },

        restoreSelection: function() {
            restoreSelection(this.savedSelection);
        },

        showAnchorForm: function (link_value) {
            this.toolbarActions.style.display = 'none';
            this.saveSelection();
            this.anchorForm.style.display = 'block';
            this.keepToolbarAlive = true;
            this.anchorInput.focus();
            this.anchorInput.value = link_value || '';
        },

        bindAnchorForm: function () {
            var linkCancel = this.anchorForm.querySelector('a'),
                self = this;
            this.anchorForm.addEventListener('click', function (e) {
                e.stopPropagation();
            });
            this.anchorInput.addEventListener('keyup', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    self.createLink(this);
                }
            });
            this.anchorInput.addEventListener('click', function (e) {
                // make sure not to hide form when cliking into the input
                e.stopPropagation();
                self.keepToolbarAlive = true;
            });
            this.anchorInput.addEventListener('blur', function () {
                self.keepToolbarAlive = false;
                self.checkSelection();
            });
            linkCancel.addEventListener('click', function (e) {
                e.preventDefault();
                self.showToolbarActions();
                restoreSelection(self.savedSelection);
            });
            return this;
        },


        hideAnchorPreview: function () {
            this.anchorPreview.classList.remove('medium-editor-anchor-preview-active');
        },

        // TODO: break method
        showAnchorPreview: function (anchorEl) {
            if (this.anchorPreview.classList.contains('medium-editor-anchor-preview-active')) {
                return true;
            }

            var self = this,
                buttonHeight = 40,
                boundary = anchorEl.getBoundingClientRect(),
                middleBoundary = (boundary.left + boundary.right) / 2,
                halfOffsetWidth,
                defaultLeft,
                timer;

            self.anchorPreview.querySelector('i').textContent = anchorEl.href;
            halfOffsetWidth = self.anchorPreview.offsetWidth / 2;
            defaultLeft = self.options.diffLeft - halfOffsetWidth;

            clearTimeout(timer);
            timer = setTimeout(function () {
                if (self.anchorPreview && !self.anchorPreview.classList.contains('medium-editor-anchor-preview-active')) {
                    self.anchorPreview.classList.add('medium-editor-anchor-preview-active');
                }
            }, 100);

            self.observeAnchorPreview(anchorEl);

            self.anchorPreview.classList.add('medium-toolbar-arrow-over');
            self.anchorPreview.classList.remove('medium-toolbar-arrow-under');
            self.anchorPreview.style.top = Math.round(buttonHeight + boundary.bottom - self.options.diffTop + window.pageYOffset - self.anchorPreview.offsetHeight) + 'px';
            if (middleBoundary < halfOffsetWidth) {
                self.anchorPreview.style.left = defaultLeft + halfOffsetWidth + 'px';
            } else if ((window.innerWidth - middleBoundary) < halfOffsetWidth) {
                self.anchorPreview.style.left = window.innerWidth + defaultLeft - halfOffsetWidth + 'px';
            } else {
                self.anchorPreview.style.left = defaultLeft + middleBoundary + 'px';
            }

            return this;
        },

        // TODO: break method
        observeAnchorPreview: function (anchorEl) {
            var self = this,
                lastOver = (new Date()).getTime(),
                over = true,
                stamp = function () {
                    lastOver = (new Date()).getTime();
                    over = true;
                },
                unstamp = function (e) {
                    if (!e.relatedTarget || !/anchor-preview/.test(e.relatedTarget.className)) {
                        over = false;
                    }
                },
                interval_timer = setInterval(function () {
                    if (over) {
                        return true;
                    }
                    var durr = (new Date()).getTime() - lastOver;
                    if (durr > self.options.anchorPreviewHideDelay) {
                        // hide the preview 1/2 second after mouse leaves the link
                        self.hideAnchorPreview();

                        // cleanup
                        clearInterval(interval_timer);
                        self.anchorPreview.removeEventListener('mouseover', stamp);
                        self.anchorPreview.removeEventListener('mouseout', unstamp);
                        anchorEl.removeEventListener('mouseover', stamp);
                        anchorEl.removeEventListener('mouseout', unstamp);

                    }
                }, 200);

            self.anchorPreview.addEventListener('mouseover', stamp);
            self.anchorPreview.addEventListener('mouseout', unstamp);
            anchorEl.addEventListener('mouseover', stamp);
            anchorEl.addEventListener('mouseout', unstamp);
        },

        createAnchorPreview: function () {
            var self = this,
                anchorPreview = document.createElement('div');

            anchorPreview.id = 'medium-editor-anchor-preview-' + this.id;
            anchorPreview.className = 'medium-editor-anchor-preview';
            anchorPreview.innerHTML = this.anchorPreviewTemplate();
            this.options.elementsContainer.appendChild(anchorPreview);

            anchorPreview.addEventListener('click', function () {
                self.anchorPreviewClickHandler();
            });

            return anchorPreview;
        },

        anchorPreviewTemplate: function () {
            return '<div class="medium-editor-toolbar-anchor-preview" id="medium-editor-toolbar-anchor-preview">' +
                '    <i class="medium-editor-toolbar-anchor-preview-inner"></i>' +
                '</div>';
        },

        anchorPreviewClickHandler: function (e) {
            if (this.activeAnchor) {

                var self = this,
                    range = document.createRange(),
                    sel = window.getSelection();

                range.selectNodeContents(self.activeAnchor);
                sel.removeAllRanges();
                sel.addRange(range);
                setTimeout(function () {
                    if (self.activeAnchor) {
                        self.showAnchorForm(self.activeAnchor.href);
                    }
                    self.keepToolbarAlive = false;
                }, 100 + self.options.delay);

            }

            this.hideAnchorPreview();
        },

        editorAnchorObserver: function (e) {
            var self = this,
                overAnchor = true,
                leaveAnchor = function () {
                    // mark the anchor as no longer hovered, and stop listening
                    overAnchor = false;
                    self.activeAnchor.removeEventListener('mouseout', leaveAnchor);
                };

            if (e.target && e.target.tagName.toLowerCase() === 'a') {

                // Detect empty href attributes
                // The browser will make href="" or href="#top"
                // into absolute urls when accessed as e.targed.href, so check the html
                if (!/href=["']\S+["']/.test(e.target.outerHTML) || /href=["']#\S+["']/.test(e.target.outerHTML)) {
                    return true;
                }

                // only show when hovering on anchors
                if (this.toolbar.classList.contains('medium-editor-toolbar-active')) {
                    // only show when toolbar is not present
                    return true;
                }
                this.activeAnchor = e.target;
                this.activeAnchor.addEventListener('mouseout', leaveAnchor);
                // show the anchor preview according to the configured delay
                // if the mouse has not left the anchor tag in that time
                setTimeout(function () {
                    if (overAnchor) {
                        self.showAnchorPreview(e.target);
                    }
                }, self.options.delay);


            }
        },

        bindAnchorPreview: function (index) {
            var i, self = this;
            this.editorAnchorObserverWrapper = function (e) {
                self.editorAnchorObserver(e);
            };
            for (i = 0; i < this.elements.length; i += 1) {
                this.elements[i].addEventListener('mouseover', this.editorAnchorObserverWrapper);
            }
            return this;
        },

        checkLinkFormat: function (value) {
            var re = /^(https?|ftps?|rtmpt?):\/\/|mailto:/;
            return (re.test(value) ? '' : 'http://') + value;
        },

        setTargetBlank: function () {
            var el = getSelectionStart(),
                i;
            if (el.tagName.toLowerCase() === 'a') {
                el.target = '_blank';
            } else {
                el = el.getElementsByTagName('a');
                for (i = 0; i < el.length; i += 1) {
                    el[i].target = '_blank';
                }
            }
        },

        createLink: function (input) {
            if (input.value.trim().length === 0) {
                this.hideToolbarActions();
                return;
            }
            restoreSelection(this.savedSelection);
            if (this.options.checkLinkFormat) {
                input.value = this.checkLinkFormat(input.value);
            }
            document.execCommand('createLink', false, input.value);
            if (this.options.targetBlank) {
                this.setTargetBlank();
            }
            this.checkSelection();
            this.showToolbarActions();
            input.value = '';
        },

        bindWindowActions: function () {
            var timerResize,
                self = this;
            this.windowResizeHandler = function () {
                clearTimeout(timerResize);
                timerResize = setTimeout(function () {
                    if (self.toolbar && self.toolbar.classList.contains('medium-editor-toolbar-active')) {
                        self.setToolbarPosition();
                    }
                }, 100);
            };
            window.addEventListener('resize', this.windowResizeHandler);
            return this;
        },

        activate: function () {
            if (this.isActive) {
                return;
            }

            this.setup();
        },

        // TODO: break method
        deactivate: function () {
            var i;
            if (!this.isActive) {
                return;
            }
            this.isActive = false;

            if (this.toolbar !== undefined) {
                this.options.elementsContainer.removeChild(this.anchorPreview);
                this.options.elementsContainer.removeChild(this.toolbar);
                delete this.toolbar;
                delete this.anchorPreview;
            }

            document.documentElement.removeEventListener('mouseup', this.checkSelectionWrapper);
            window.removeEventListener('resize', this.windowResizeHandler);

            for (i = 0; i < this.elements.length; i += 1) {
                this.elements[i].removeEventListener('mouseover', this.editorAnchorObserverWrapper);
                this.elements[i].removeEventListener('keyup', this.checkSelectionWrapper);
                this.elements[i].removeEventListener('blur', this.checkSelectionWrapper);
                this.elements[i].removeEventListener('paste', this.pasteWrapper);
                this.elements[i].removeAttribute('contentEditable');
                this.elements[i].removeAttribute('data-medium-element');
            }

        },

        htmlEntities: function (str) {
            // converts special characters (like <) into their escaped/encoded values (like &lt;).
            // This allows you to show to display the string without the browser reading it as HTML.
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        },

        bindPaste: function () {
            var i, self = this;
            this.pasteWrapper = function (e) {
                var paragraphs,
                    html = '',
                    p;

                this.classList.remove('medium-editor-placeholder');
                if (!self.options.forcePlainText && !self.options.cleanPastedHTML) {
                    return this;
                }

                if (e.clipboardData && e.clipboardData.getData && !e.defaultPrevented) {
                    e.preventDefault();

                    if (self.options.cleanPastedHTML && e.clipboardData.getData('text/html')) {
                        return self.cleanPaste(e.clipboardData.getData('text/html'));
                    }
                    if (!(self.options.disableReturn || this.getAttribute('data-disable-return'))) {
                        paragraphs = e.clipboardData.getData('text/plain').split(/[\r\n]/g);
                        for (p = 0; p < paragraphs.length; p += 1) {
                            if (paragraphs[p] !== '') {
                                if (navigator.userAgent.match(/firefox/i) && p === 0) {
                                    html += self.htmlEntities(paragraphs[p]);
                                } else {
                                    html += '<p>' + self.htmlEntities(paragraphs[p]) + '</p>';
                                }
                            }
                        }
                        document.execCommand('insertHTML', false, html);
                    } else {
                        document.execCommand('insertHTML', false, e.clipboardData.getData('text/plain'));
                    }
                }
            };
            for (i = 0; i < this.elements.length; i += 1) {
                this.elements[i].addEventListener('paste', this.pasteWrapper);
            }
            return this;
        },

        setPlaceholders: function () {
            var i,
                activatePlaceholder = function (el) {
                    if (!(el.querySelector('img')) &&
                            !(el.querySelector('blockquote')) &&
                            el.textContent.replace(/^\s+|\s+$/g, '') === '') {
                        el.classList.add('medium-editor-placeholder');
                    }
                },
                placeholderWrapper = function (e) {
                    this.classList.remove('medium-editor-placeholder');
                    if (e.type !== 'keypress') {
                        activatePlaceholder(this);
                    }
                };
            for (i = 0; i < this.elements.length; i += 1) {
                activatePlaceholder(this.elements[i]);
                this.elements[i].addEventListener('blur', placeholderWrapper);
                this.elements[i].addEventListener('keypress', placeholderWrapper);
            }
            return this;
        },

        cleanPaste: function (text) {

            /*jslint regexp: true*/
            /*
                jslint does not allow character negation, because the negation
                will not match any unicode characters. In the regexes in this
                block, negation is used specifically to match the end of an html
                tag, and in fact unicode characters *should* be allowed.
            */
            var i, elList, workEl,
                el = this.getSelectionElement(),
                multiline = /<p|<br|<div/.test(text),
                replacements = [

                    // replace two bogus tags that begin pastes from google docs
                    [new RegExp(/<[^>]*docs-internal-guid[^>]*>/gi), ""],
                    [new RegExp(/<\/b>(<br[^>]*>)?$/gi), ""],

                     // un-html spaces and newlines inserted by OS X
                    [new RegExp(/<span class="Apple-converted-space">\s+<\/span>/g), ' '],
                    [new RegExp(/<br class="Apple-interchange-newline">/g), '<br>'],

                    // replace google docs italics+bold with a span to be replaced once the html is inserted
                    [new RegExp(/<span[^>]*(font-style:italic;font-weight:bold|font-weight:bold;font-style:italic)[^>]*>/gi), '<span class="replace-with italic bold">'],

                    // replace google docs italics with a span to be replaced once the html is inserted
                    [new RegExp(/<span[^>]*font-style:italic[^>]*>/gi), '<span class="replace-with italic">'],

                    //[replace google docs bolds with a span to be replaced once the html is inserted
                    [new RegExp(/<span[^>]*font-weight:bold[^>]*>/gi), '<span class="replace-with bold">'],

                     // replace manually entered b/i/a tags with real ones
                    [new RegExp(/&lt;(\/?)(i|b|a)&gt;/gi), '<$1$2>'],

                     // replace manually a tags with real ones, converting smart-quotes from google docs
                    [new RegExp(/&lt;a\s+href=(&quot;|&rdquo;|&ldquo;|“|”)([^&]+)(&quot;|&rdquo;|&ldquo;|“|”)&gt;/gi), '<a href="$2">']

                ];
            /*jslint regexp: false*/

            for (i = 0; i < replacements.length; i += 1) {
                text = text.replace(replacements[i][0], replacements[i][1]);
            }

            if (multiline) {

                // double br's aren't converted to p tags, but we want paragraphs.
                elList = text.split('<br><br>');

                this.pasteHTML('<p>' + elList.join('</p><p>') + '</p>');
                document.execCommand('insertText', false, "\n");

                // block element cleanup
                elList = el.querySelectorAll('p,div,br');
                for (i = 0; i < elList.length; i += 1) {

                    workEl = elList[i];

                    switch (workEl.tagName.toLowerCase()) {
                    case 'p':
                    case 'div':
                        this.filterCommonBlocks(workEl);
                        break;
                    case 'br':
                        this.filterLineBreak(workEl);
                        break;
                    }

                }


            } else {

                this.pasteHTML(text);

            }

        },

        pasteHTML: function (html) {
            var elList, workEl, i, fragmentBody, pasteBlock = document.createDocumentFragment();

            pasteBlock.appendChild(document.createElement('body'));

            fragmentBody = pasteBlock.querySelector('body');
            fragmentBody.innerHTML = html;

            this.cleanupSpans(fragmentBody);

            elList = fragmentBody.querySelectorAll('*');
            for (i = 0; i < elList.length; i += 1) {

                workEl = elList[i];

                // delete ugly attributes
                workEl.removeAttribute('class');
                workEl.removeAttribute('style');
                workEl.removeAttribute('dir');

                if (workEl.tagName.toLowerCase() === 'meta') {
                    workEl.parentNode.removeChild(workEl);
                }

            }
            document.execCommand('insertHTML', false, fragmentBody.innerHTML.replace(/&nbsp;/g, ' '));
        },
        isCommonBlock: function (el) {
            return (el && (el.tagName.toLowerCase() === 'p' || el.tagName.toLowerCase() === 'div'));
        },
        filterCommonBlocks: function (el) {
            if (/^\s*$/.test(el.innerText)) {
                el.parentNode.removeChild(el);
            }
        },
        filterLineBreak: function (el) {
            if (this.isCommonBlock(el.previousElementSibling)) {

                // remove stray br's following common block elements
                el.parentNode.removeChild(el);

            } else if (this.isCommonBlock(el.parentNode) && (el.parentNode.firstChild === el || el.parentNode.lastChild === el)) {

                // remove br's just inside open or close tags of a div/p
                el.parentNode.removeChild(el);

            } else if (el.parentNode.childElementCount === 1) {

                // and br's that are the only child of a div/p
                this.removeWithParent(el);

            }

        },

        // remove an element, including its parent, if it is the only element within its parent
        removeWithParent: function (el) {
            if (el && el.parentNode) {
                if (el.parentNode.parentNode && el.parentNode.childElementCount === 1) {
                    el.parentNode.parentNode.removeChild(el.parentNode);
                } else {
                    el.parentNode.removeChild(el.parentNode);
                }
            }
        },

        cleanupSpans: function (container_el) {

            var i,
                el,
                new_el,
                spans = container_el.querySelectorAll('.replace-with');

            for (i = 0; i < spans.length; i += 1) {

                el = spans[i];
                new_el = document.createElement(el.classList.contains('bold') ? 'b' : 'i');

                if (el.classList.contains('bold') && el.classList.contains('italic')) {

                    // add an i tag as well if this has both italics and bold
                    new_el.innerHTML = '<i>' + el.innerHTML + '</i>';

                } else {

                    new_el.innerHTML = el.innerHTML;

                }
                el.parentNode.replaceChild(new_el, el);

            }

            spans = container_el.querySelectorAll('span');
            for (i = 0; i < spans.length; i += 1) {

                el = spans[i];

                // remove empty spans, replace others with their contents
                if (/^\s*$/.test()) {
                    el.parentNode.removeChild(el);
                } else {
                    el.parentNode.replaceChild(document.createTextNode(el.innerText), el);
                }

            }

        }

    };

}(window, document));


(function(){


;

/**
 * @module Namespaces
**/

window.UI = {};
UI.Components = {};


UI._UID = 0;
UI.UID = function(){
	UI._UID += 1;
	return UI._UID.toString();
};

/**
 * @module Component
* @requires Namespaces
 */

UI.Component = Backbone.View.extend({
	defaultOptions :{
		_classes:"ui",
		classes:"" 
	},
	tagName:"div",
	_renderedOnce:false,
	_selfModel:false,
	initialize:function(options){
		
		this.options = _.extend({}, this.defaultOptions, options || {}) 


		this.model = new Backbone.Model();

	},

	render:function(){
		this.$el.empty();
		this.$el.addClass(this.options._classes+" "+this.options.classes);
		if(typeof this.options.id != 'undefined'){
			this.$el.attr("id", this.options.id);
		}

		if(typeof this.options.value != 'undefined'){
			this.$el.attr("data-value", this.options.value);
		}
		this._renderedOnce = true;
		return this;
	}
});;

/**
 * @module Button
 * @requires Component
 */
UI.Components.Button = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		$text:null,

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui button",
				classes:"",
				label:"",
				value:"",
				button:true
			}, options || {})]);


		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			if(this.options.button == false){
				this.$el.removeClass("button");
			}
			if(typeof this.options.tabindex != 'undefined'){
				this.$el.attr("tabindex", this.options.tabindex);
			}else{
				this.$el.attr("tabindex",0);
			}
			this.$text = $("<span/>");
			this.$text.text(this.options.label);
			this.$el.append(this.$text);
			
			if(typeof this.options.iconLeft != 'undefined'){
				this.$text.before( $("<i/>", {"class":this.options.iconLeft}) );
				this.$el.addClass("icon-left");
			}
			if(typeof this.options.iconRight != 'undefined'){
				this.$text.after( $("<i/>", {"class":this.options.iconRight}) );
				this.$el.addClass("icon-right");
			}
			if(typeof this.options.icon != 'undefined'){
				this.$el.append( $("<i/>",{"class":this.options.icon}) );
			}
			if(typeof this.options.href != 'undefined'){
				this.$el.attr("href", this.options.href);
			}


			if(typeof this.options.title != 'undefined'){
				this.$el.attr("title", this.options.title);
			}

			this.$el.click($.proxy(function(e) {
				this.clickEvent(e);
		    },this));

			this.$el.keypress($.proxy(function(e) {
		        if(e.which == 13) {
		            this.clickEvent(e);
		        }
		    },this));
				
			
			//button.on("click", setSelected, this);
			return this;
		},
		clickEvent:function(e){
			// if(typeof this.options.pushState != 'undefined' && this.options.pushState == true){
			// 	e.preventDefault();
			// 	if(typeof this.options.defaultHref != 'undefined' && this.options.defaultHref != null){
			// 		Backbone.history.navigate( this.options.defaultHref, true );
			// 	}else{
			// 		Backbone.history.navigate( this.options.href, true );
			// 	}
				
			// }
			if(typeof this.options.click != 'undefined'){
				this.options.click();
			}
			this.trigger("click", this.options);
		},
		/**
		@inner
		@memberof UI.Button
		@method active
		@param value {boolean}
		*/
		active:function(bool){
			this.$el.toggleClass("active", bool);
		},
		/**
		@inner
		@memberof UI.Button
		@method highlight
		@param value {boolean}
		*/
		highlight:function(bool){
			this.$el.toggleClass("highlight", bool);
		},
		
		_disabled:false,
		/**
		@inner
		@memberof UI.Button
		@method disabled
		@param value {boolean} Optional
		@returns {Boolean}
		*/
		disabled:function(bool){
			if(bool != undefined){
				this._disabled = bool;
				this.$el.toggleClass("disabled", this._disabled);
			}
			return this._disabled;
		},
		editLabel:function(){
			this.$text.attr("contenteditable", "true");
			this.$text.selectText();
			var self = this;
			this.$text.bind("keypress", function(e){
				if(e.keyCode==13) { //Enter keycode
					e.preventDefault();
					self.finishEditingLabel();
				}
			});
			setTimeout($.proxy(function(){
				
				this.$text.bind("clickoutside", $.proxy(function(){
					this.finishEditingLabel();
				},this));
			},this),100);

		},
		setLabel:function(labelString){
			this.$text.text(labelString);
		},
		setIcon:function(classString){
			this.options.icon = classString;
			this.$el.find("i").attr("class", classString);
		},
		cleanEditingLabel:function(){
			this.$text.off("clickoutside");
			this.$text.off("keypress");
			this.$text.attr("contenteditable", "false");
		},
		cancelEditingLabel:function(){
			this.$text.text(this.options.label);
		},	
		finishEditingLabel:function(){
			if($.trim(this.$text.text()) == "" ){
				this.cancelEditingLabel();
			}else{
				this.options.label = this.$text.text();
				this.trigger("edit", this.options);
			}
			
			this.cleanEditingLabel();
		}
	})

};
;

/**
 * @module Calendar
 * @requires Component
 */
UI.Components.Calendar = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui calendar",
				classes:"",
				label:"",
				value:"",
				button:true
			}, options || {})]);


		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			// Array to store month names
			var months = new Array('January',' February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'); 

			// Array to store month days
			var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

			// Array to store week names
			var weekDay = new Array('Su','Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa');

			var date = new Date();

			var day = date.getDate();
			var month = date.getMonth();
			var year = date.getFullYear();
			var days_in_this_month = monthDays[month];
			var first_week_day = new Date(year, month, 1).getDay();

			// Define the variable which holds the calendar table
			  var calendar_html = '';

			  // Create row for displaying current month and year
			 // calendar_html += '<tr><td class="monthHead" colspan="7">' + months[month] + ' ' + year + '</td></tr>';
			  
			  calendar_html += '<div class="header"><div class="previous"><i class="fa fa-chevron-left fa-fw"></i></div><div class="title">'+months[month]+'</div><div class="next"><i class="fa fa-chevron-right fa-fw"></i></div></div>';

			  calendar_html += '<div class="days">';
			  // Create row to display the week days (Monday - Sunday)
			  calendar_html += '<div class="week">';
			  for(week_day= 0; week_day < 7; week_day++) {
			        calendar_html += '<div class="day-display"><div class="date">' + weekDay[week_day] + '</div></div>';
			  }
			  calendar_html += '</div>';


			  // Fill first row of the month with empty cells until the month starts.
			  calendar_html += '<div class="week">';
			  for(week_day = 0; week_day < first_week_day; week_day++) {
			        calendar_html += '<div class="day"> </div>';
			  }

			  // Populate the days in the month
			  week_day = first_week_day;
			  for(day_counter = 1; day_counter <= days_in_this_month; day_counter++) {

			        week_day %= 7;
			        // Check if week ends, if yes move to next row
			        if(week_day == 0){
			        	calendar_html += '</div><div class="week">';
			        }
			        // Show the current day in bold.
			        if(day == day_counter){
			          calendar_html += '<div class="day clickable today"><div class="date">' + day_counter + '</div></div>';
			        }else{
			          calendar_html += '<div class="day clickable"><div class="date">' + day_counter + '</div></div>';
			        }

			        week_day++;
			  }

			  //Add remaining days
			  for(var i = 0; i < (7-week_day); i++){
			  		calendar_html += '<div class="day"><div class="date">' + "" + '</div></div>';
			  }

			  //close week
			  calendar_html += '</div>';
			  
			  // Close the Calendar table
			  calendar_html += '</div>';
			  calendar_html += '';

			  // Display the calendar.
			  this.$el.html(calendar_html);

		

			return this;
		}
	})

};
;

/**
 * @module Checkbox
 * @requires Component
 */
UI.Checkbox = UI.Component.extend({
	options:{
		_classes:"ui checkbox",
		classes:"",
		bind:""
	},
	$checkbox:null,
	initialize:function(options){

		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui checkbox",
			classes:"",
		}, options || {})]);

		this.model.set("value", false);
		if(typeof this.options.value != 'undefined'){
			this.model.set("value", this.options.value);
		}

		if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
			this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
				this.model.set("value", this.options.model.get(this.options.bind));
			});
			if(typeof this.options.value == 'undefined'){
				this.model.set("value", this.options.model.get(this.options.bind));
			}
		}

		this.listenTo(this.model, "change:value", function(e,value){
			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				if(typeof this.options.property != 'undefined'){
					this.options.model.set(this.options.bind, this.value());
				}else{
					this.options.model.set(this.options.bind, this.model.get("value"));
				}
			}
			this.value(this.model.get("value"));
		});

		this.options.checkboxID = "ui-checkboc-"+UI.UID();


	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$checkbox = $("<input/>", {
			type:"checkbox",
			id:this.options.checkboxID
		});
		this.$el.append(this.$checkbox);
		this.$el.attr("tabindex", "0");
		this.$el.keypress($.proxy(function(e) {
	        if(e.which == 13) {
				if(this.model.get("value") == true){
					this.value(false);
				}else{
					this.value(true);
				}
	        }
	    },this));

		this.$label = $("<label/>", {
			"class":"label",
			"for":this.options.checkboxID
		});
		this.$el.append(this.$label);
		if(typeof this.options.label != 'undefined'){
			this.$label.text(this.options.label);
		}else{
			this.$el.addClass("no-label");
		}

		var self = this;
		this.$checkbox.change(function(){
			self.valueChange();
		});

		this.value(this.model.get("value"));

		return this;
	},
	valueChange:function(){
		this.model.set("value", this.$checkbox.prop('checked'));
	},
	value:function(value){
		if(typeof value != 'undefined'){
			this.model.set("value", value);
			this.$checkbox.prop('checked', value);
		}
		return this.model.get("value");
	}
});;

/**
 * @module Collection
 * @requires Component,Button
 */
UI.Collection = UI.Component.extend({
		group:[],
		$itemContainer:null,
		itemCollection:null,
		selectedItem:null,
		selectedModel:null,
		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui collection",
				classes:"",
				align:"center",
				labelProperty:"label",
				value:null,
				items:[]
			}, options || {})]);

			this.model.set("value", null);
			if(this.options.value != null){
				this.model.set("value", this.options.value);
			}

			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
					this.model.set("value", this.options.model.get(this.options.bind));
				});
				if(typeof this.options.value == 'undefined'){
					this.model.set("value", this.options.model.get(this.options.bind));
				}
			}

			this.listenTo(this.model, "change:value", function(e,value){
				this.setSelected();
				if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){

					if(typeof this.options.property != 'undefined'){
						this.options.model.set(this.options.bind, this.value());
					}else{
						this.options.model.set(this.options.bind, this.model.get("value"));
					}
				}
			});

			if(typeof this.options.items.each == 'undefined'){
				this.items = new Backbone.Collection(this.options.items);
			}else{
				this.items = this.options.items;
			}
			this.items.on("all", function(e,value){
				this.renderItems();
			},this);

		},
		beforeRender:function(){
			this.$el.addClass("align-"+this.options.align);

			if(typeof this.options.label != 'undefined'){
				var label = new UI.Label({
					text:this.options.label
				}).render();
				this.$el.append(label.$el);
			}

			this.createItemContainer();

		},
		createItemContainer:function(){ 
			this.$itemContainer = $("<div/>", {"class":"items"});
		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			this.beforeRender();
			
			this.$el.append(this.$itemContainer);

			this.renderItems();

			this.value(this.model.get("value"));

			return this;
		},
		renderItem:function(item){
			var itemObject = _.clone(item.attributes);
			itemObject.model = item;
			itemObject.label = item.get(this.options.labelProperty);

			var button = new UI.Button(itemObject).render();
			button.on("edit", function(e){
				e.options.model.set("label", e.label);
			},this);

			button.on("click", this.itemClick, this);

			return button;
		},
		renderItems:function(){

			this.$itemContainer.empty();
			this.group = [];
			this.selectedItem = null;
			this.items.each(function(item){
				if(typeof this.options.itemClasses != 'undefined'){
					if(item.attributes.classes == 'undefined'){
						item.attributes.classes = this.options.itemClasses;
					}else{
						item.attributes.classes = item.attributes.classes+" "+this.options.itemClasses;
					}
				}
				var button = this.renderItem(item);
				if(typeof button.$el == 'undefined'){
					this.$itemContainer.append(button);
				}else{
					this.$itemContainer.append(button.$el);
				}
				this.group.push(button);

			},this);

			if(this.selectedItem != null && typeof this.selectedItem != 'undefined'){
				var self = this;
				setTimeout(function(){
					if(self.selectedItem.$el.position().top > (self.$itemContainer.scrollTop()+self.$itemContainer.height())){
						self.$itemContainer.scrollTop(self.selectedItem.$el.position().top);
					}
				},100);
			}

			this.setSelected();

		},


		/**
		@inner
		@memberof UI.List
		@method setSelected
		@param value {Bakcbone.Modal}
		*/ 
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}	

			if(this.options.property != null){ //match on the options.property
				if(valueIsModel){
					value = value.get(this.options.property);
				}
				_.each(this.group, function(item){
					if(item.options.model.get(this.options.property) === value){
						item.active(true);
						this.selectedItem = item;
					}else{
						item.active(false);
					}
				},this);

			}else if(typeof value.attributes != 'undefined'){ //just match model equality
				_.each(this.group, function(item){
					if(item.options.model == value){
						item.active(true);
						this.selectedItem = item;
					}else{
						item.active(false);
					}
				},this);
			}

		},
		value:function(newValue){
			if(typeof newValue != 'undefined' && newValue != null){

				if(typeof this.options.property != 'undefined' && typeof newValue.attributes == 'undefined'){
					var findObj = {};
					findObj[this.options.property] = newValue;
					this.model.set("value", this.items.findWhere(findObj).get(this.options.property) );
				}else{
					this.model.set("value", newValue);
				}

			}

			return this.model.get("value");

		}


});

;

/**
 * @module Label
 * @requires Component
 */
UI.Label = UI.Component.extend({

	$text:null,
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui label",
			classes:"",
			text:"Label",
			style:"span",
			accessoryRight:null
		}, options || {})]);


		if(typeof this.options.model != 'undefined'){
			this.model = this.options.model;
		}else{
			this.model = new Backbone.Model();
		}
		this.listenTo(this.model, "change", function(){
			this.updateText();
		});

	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$text = $("<"+this.options.style+"/>"); 
		this.$el.append(this.$text);
		

		if(this.options.accessoryRight != null){
			this.accessoryRight = $("<div/>", {"class":"accessory-right"});
			this.accessoryRight.append(this.options.accessoryRight);
			this.$el.prepend(this.accessoryRight);
		}

		this.updateText();

		//button.on("click", setSelected, this);
		return this;
	},
	updateText:function(){
		if(typeof this.model != 'undefined' && typeof this.options.bind != 'undefined'){

			if(typeof this.options.value != 'undefined'){
				this.text(this.model.get(this.options.bind).get(this.options.value));
			}else{
				this.text(this.model.get(this.options.bind));
			}
			
		}else{
			this.text(this.options.text);
		}
	},
	text:function(str){
		if(str != null && typeof str != 'undefined'){
			this.options.text = str;
			this.$text.text(this.options.text);
		}
		return this.options.text;
	},
	edit:function(){

	}
});;

/**
 * @module List
 * @requires Component,Collection
 */
UI.Components.List = {
	init:function(){
		// ElementQuery.add({
		// 	"this-min-width":640,
		// 	class:"horizontal",
		// 	selector:".ui.switch"
		// });
	},  
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui list"
			}, options || {})]);

			//console.log("Switch", this.options.items);

		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.call(this);
			this.$itemContainer.css("-webkit-overflow-scrolling", "touch");
		},
		renderItems:function(){
			UI.Collection.prototype.renderItems.call(this);

		},
		itemClick:function(event){
			if(this.options.property != null){
				this.value(event.model.get(this.options.property));
			}else{
				this.value(event.model);
			}
			this.trigger("select");
		},


	})
};

;

/**
 * @module Select
 * @requires Component,Collection
 */
UI.Components.Select = {
	init:function(){
		
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui select",
				property:"cid"
			}, options || {})]);
		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.apply(this);
		},
		createItemContainer:function(){
			this.$itemContainer = $("<select/>", {"class":"items"});
			this.$itemContainer.change($.proxy(function(){
				if(this.options.property != "cid"){ //set value as a primitive
					this.value(this.group[this.$itemContainer.val()].get(this.options.property));
				}else{ //set value as a model
					this.value(this.group[this.$itemContainer.val()]);
				}
			},this));
		},
		renderItem:function(item){
			var itemValue;
			if(this.options.property == "cid"){
				itemValue = item.cid
			}else{
				itemValue = item.get(this.options.property).toString();
			}
			var $option = $("<option/>", {text:item.get("label"), value:itemValue });
			this.group[itemValue] = item;

			return $option;
		},
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}	

			if(valueIsModel){
				this.$itemContainer.val(value.cid);
			}else{
				this.$itemContainer.val(value);
			}

		}


	})
};













;

/**
 * @module Tabs
 * @requires Component,Button
 */
UI.Tabs = UI.Component.extend({

	tabs:{},
	tabsCollection:null,
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui tabs",
			classes:"",
			bind:"",
		}, options || {})]);
		
		this.model.set("selected", "0");

		this.model.on("change", function(e,value){
			this.setSelected(e.get("selected"));
		},this);

		if(typeof this.options.tabs.each == 'undefined'){
			this.tabsCollection = new Backbone.Collection(options.tabs);
			this.tabsCollection.on("change", function(e,value){
				this.renderTabs();
			},this);
		}else{
			this.tabsCollection = this.options.items;
		}


	},	
	render:function(){
		UI.Component.prototype.render.apply(this);

		if(typeof this.options.label != 'undefined'){
			var label = new UI.Label({
				text:this.options.label
			}).render();
			this.$el.append(label.$el);
		}

		this.$tabsButtonsContainer = $("<div/>", {"class":"tab-buttons"});
		this.$el.append(this.$tabsButtonsContainer);

		this.$tabsContentContainer = $("<div/>", {"class":"tab-content"});
		this.$el.append(this.$tabsContentContainer);

		this.renderTabs();

		return this;
	},
	renderTabs:function(){

		this.tabs = {};
		this.$tabsButtonsContainer.empty();
		this.$tabsContentContainer.empty();
		var firstTab = null;

		this.tabsCollection.each(function(item){
			if(typeof item.attributes != 'undefined'){
				item = item.attributes;
			}

			item.value = "tab_"+UI.UID();
			if(firstTab == null){
				firstTab = item.value;
			}

			var button = new UI.Button({
				label:item.label,
				value:item.value
			}).render();
			button.on("click", this.itemClick, this);


			if(item.value == this.model.get(this.options.bind)){
				button.active(true);
				selectedItem = button;
			}


			var $tab = $("<div/>", {"data-value":item.value});
			if(typeof item.content != 'undefined'){
				$tab.append(item.content);					
			}
			

			this.$tabsButtonsContainer.append(button.$el);
			this.$tabsContentContainer.append($tab);

			this.tabs[item.value] = {
				button:button,
				content:$tab
			}


		},this);

		this.setSelected(firstTab);
		


	},

	itemClick:function(event){
		this.model.set("selected", event.value);
		this.trigger("select", event);
	},
	setSelected:function(value){
		this.$('.tab-content>div.active, .tab-buttons>div.active').toggleClass("active", false);
		this.$('.tab-content>div[data-value="'+value+'"], .tab-buttons>div[data-value="'+value+'"]').toggleClass("active", true);
	}

});

;

/**
 * @module Switch
 * @requires Component,Collection
 */
UI.Components.Switch = {
	init:function(){
		ElementQuery.add({
			"parent-min-width":640,
			"class":"horizontal",
			selector:".ui.switch"
		}); 
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui switch"
			}, options || {})]);

		}, 
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.call(this);
			this.$itemContainer.css("-webkit-overflow-scrolling", "touch");
		},
		renderItems:function(){
			UI.Collection.prototype.renderItems.call(this);

		},
				itemClick:function(event){
			if(this.options.property != null){
				this.value(event.model.get(this.options.property));
			}else{
				this.value(event.model);
			}
			this.trigger("select");
		},


	})
};

;

/**
 * @module Form
 * @requires Component,Button,Label,List,Select,Tabs,Switch,Checkbox
 */
UI.Components.Form = {
	init:function(){
		ElementQuery.add({
			"this-min-width":500,
			"class":"large",
			selector:".ui.form"
		});
	},
	constructor: UI.Component.extend({

		_elementCount:0,
		elements:{},

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui form",
				classes:"",
				structure:[]
			}, options || {})]);

		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			this.renderItems(this.options.structure);

			return this;
		},
		renderItems:function(items){

			_.each(items, function(subItems){
				this.$el.append(this.createRow(subItems));
			},this);

		},
		createRow:function(section){
			var returnedEl = null;
			if(typeof section ==  'undefined' || section == null){

			}else{
				if(typeof section.length != 'undefined'){

					returnedEl = $("<div/>", {"class":"ui row"});
					_.each(section, function(child){
						returnedEl.append(this.createRow(child));
					},this);

				}else if(typeof section == "object"){

					returnedEl = this.createCell(section);

				}
			}

			return returnedEl;
		},
		createCell:function(component){
			if(typeof component.model == 'undefined' && typeof this.options.model != 'undefined'){
				component.model = this.options.model;
			}
			if(typeof component.columns == 'undefined'){
				component.columns = "1-1";
			}

			var cellClasses = "";
			if(typeof component.cellClasses != 'undefined'){
				cellClasses = component.cellClasses;
			}
			var columns = "col-"+component.columns;
			var $cell = $("<div/>", {"class":"ui cell "+columns+" "+cellClasses});
			var newComponent = new UI[component.type](component).render();

			if(typeof component.name == 'undefined'){
				component.name = component.type+this._elementCount;
				this._elementCount++;
			}

			this.elements[component.name] = newComponent;

			$cell.append(newComponent.$el);
			return $cell;
		}
	})
}
;

/**
 * @module Input
 * @requires Component
 */
UI.Input = UI.Component.extend({
	
	$input:null,
	_focused:false,
	events:{
		//"click":"clickEvent"
	},
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [options]);
		var defaultOptions = {
			_classes:"ui input",
			classes:"",
			placeholder:"",
			debounceDelay:250,
			inputType:"text"
		};
		if(typeof options == 'undefined'){options = {};}
		_.extend(this.options, defaultOptions, options);

		this.model.set("value", "");

		if(typeof this.options.model != 'undefined'){
			this.options.model.on("change:"+this.options.bind, function(){
				if(!this._focused){
					this.$input.val(this.options.model.get(this.options.bind));
				}
			},this);
		}

	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		if(typeof this.options.field != 'undefined'){
			this.options.inputType = this.options.field;
		}
		if(this.options.inputType == "input"){
			this.options.inputType = "text";
		}

		if(typeof this.options.label != 'undefined'){
			$label = $("<div/>", {text:this.options.label, "class":"ui label"});
			this.$el.append($label);
		}

		if(typeof this.options.iconLeft != 'undefined'){
			this.$el.append( $("<i/>", {"class":this.options.iconLeft}) );
			this.$el.addClass("icon-left"); 
		}
		if(typeof this.options.iconRight != 'undefined'){
			this.$el.append( $("<i/>", {"class":this.options.iconRight}) );
			this.$el.addClass("icon-right");
		}

		if(this.options.inputType == "textarea"){
			this.$input = $("<textarea/>",{

			});
		}else{
			this.$input = $("<input/>",{
				type:this.options.inputType,
				placeholder:this.options.placeholder
			});
		}




		this.$input.on("focusin", $.proxy(function(){
			this._focused = true;
		},this));
		this.$input.on("focusout", $.proxy(function(){
			this._focused = false;
		},this));

		this.$el.append(this.$input);

		var self = this;
		if(this.options.debounce == true){
			this.$input.on("change keyup keydown",_.debounce(function(e){
				self.inputChange();
				if(e.keyCode == 13){
					self.model.trigger("change")
					if(typeof self.options.model != 'undefined'){
						self.options.model.trigger("change:"+self.options.bind);
					}
				}
			},this.options.debounceDelay));
		}else{
			this.$input.on("change keyup keydown",function(e){
				self.inputChange();
				if(e.keyCode == 13){
					self.model.trigger("change")
					if(typeof self.options.model != 'undefined'){
						self.options.model.trigger("change:"+self.options.bind);
					}
				}
			});
		}

		if(typeof this.bind != 'undefined' && typeof this.options.model != 'undefined'){
			this.$input.val(this.options.model.get(this.options.bind));
		}

		if(typeof this.options.value != 'undefined'){
			this.val(this.options.value);
		}

		return this;
	},
	inputChange:function(){
		if(typeof this.options.model != 'undefined'){
			this.options.model.set(this.options.bind, this.$input.val());
		}
	},
	active:function(bool){
		this.$el.toggleClass("active", bool);
	},
	focus:function(){
		this.$input.focus();
	},
	val:function(value, silient){
		if(typeof value != 'undefined'){
			this.$input.val(value)
			if(silient != true && typeof this.options.model != 'undefined'){
				this.options.model.set(this.options.bind, value);
			}
		}

		return this.$input.val();
	}
});;

/**
 * @module Radio
 * @requires Component
 */
UI.Radio = UI.Component.extend({
	options:{
		_classes:"ui radio",
		classes:"",
		bind:""
	},
	$radio:null,
	initialize:function(options){

		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui radio",
			classes:"",
			label:"",
		}, options || {})]);

		if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
			this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
				this.model.set("value", this.options.model.get(this.options.bind));
			});
			if(typeof this.options.value == 'undefined'){
				this.model.set("value", this.options.model.get(this.options.bind));
			}
		}

		this.listenTo(this.model, "change:value", function(e,value){
			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				if(typeof this.options.property != 'undefined'){
					this.options.model.set(this.options.bind, this.value());
				}else{
					this.options.model.set(this.options.bind, this.model.get("value"));
				}
			}
			//this.value(this.model.get("value"));
		});

		this.options.id = "ui-radio-"+UI.UID();
		if(typeof this.options.name == 'undefined'){
			this.options.name = "ui-radio-"+UI.UID();
		}

		// if(this.options.value == 'undefined'){
		// 	this.options.value = this.options.id;
		// }


	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$radio = $("<input/>", {
			type:"radio",
			id:this.options.id,
			name:this.options.name,
			value:this.options.value.toString()
		});
		this.$el.append(this.$radio);
		this.$el.attr("tabindex", "0");
		this.$el.keypress($.proxy(function(e) {
	        if(e.which == 13) {
				this.active(true);
	        }
	    },this));

	    this.$el.click($.proxy(function(){
	    	this.active(true);
	    },this) );

		if(typeof this.options.label != 'undefined'){
			this.$label = $("<label/>", {
				"class":"label",
				"for":this.options.id
			});
			this.$label.text(this.options.label);
			this.$el.append(this.$label);
		}

		var self = this;
		this.$radio.change(function(){
			self.valueChange();
		});
		
		//this.value(this.model.get("value"));

		return this;
	},
	valueChange:function(){
		this.model.set("value", this.$radio.prop('checked'));
	},
	active:function(value){
		if(typeof value != 'undefined'){
			if(value == true){
				if(this.options.model != 'undefined'){
					this.options.model.set("value", this.options.value);
				}
				this.$radio.prop('checked', value);
			}
		}
		return this.$radio.prop('checked');
	}
});;

/**
 * @module RadioGroup
 * @requires Component,Collection
 */
UI.Components.RadioGroup = {
	init:function(){
		
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui radio-group",
				property:"cid"
			}, options || {})]);

			this.options.radioID = "ui-radio-group-"+UI.UID();
		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.apply(this);
		},
		createItemContainer:function(){
			this.$itemContainer = $("<div/>", {"class":"items"});
			// this.$itemContainer.change($.proxy(function(){
			// 	if(this.options.property != "cid"){ //set value as a primitive
			// 		this.value(this.group[this.$itemContainer.val()].get(this.options.property));
			// 	}else{ //set value as a model
			// 		this.value(this.group[this.$itemContainer.val()]);
			// 	}
			// },this));
		},
		renderItem:function(item){
			item = item.attributes;
			item.name = this.options.radioID;
			item.model = this.model;

			var $option = new UI.Radio(item).render();
			this.group[item.value] = $option;

			return $option.$el;
		},
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}

			if(typeof this.group[value] != 'undefined'){
				this.group[value].active(true);
			}




			// if(valueIsModel){
			// 	//this.$itemContainer.val(value.cid);
			// }else{
			// 	this.$itemContainer.children()
			// 	//this.$itemContainer.val(value);
			// }

		}


	})
};













;

/**
 * @module Toolbar
 * @requires Component,Button
 */ 
UI.Components.Toolbar = {
	init:function(){
		/*
		ElementQuery.add({
			"this-min-width":640,
			class:"large",
			selector:".ui.form"
		});
*/
	},
	constructor: UI.Component.extend({
		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui toolbar",
				classes:"horizontal",
				id:""
			}, options || {})]);
		},
		render:function(){
			UI.Component.prototype.render.apply(this);
			return this;
		},
		add:function(element, options){
			if(typeof options == 'undefined'){options = {}};
			if(typeof options["class"] == 'undefined') { options["class"] = "" };
			var $wrapper = $('<div class="item '+options["class"]+'"></div>');
			if(typeof element.$el != 'undefined'){
				$wrapper.append(element.$el);
			}else{
				$wrapper.append(element);
			}
			this.$el.append($wrapper);
		}
	})
};

/**
 * @module RichText
 * @requires Component,Button,Toolbar,List
 */ 
UI.Components.RichText = {
	$editRegion:null,
	mediumEditor:null,
	isFocused:false,
	init:function(){
		ElementQuery.add({
			"this-min-width":640,
			"class":"large",
			selector:".ui.rich-text"
		});
	},
	constructor: UI.Component.extend({

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui rich-text",
				classes:"",
				id:"",
				placeholder:"",
				bind:"content"
			}, options || {})]);

			this.model.set("value", "");
			if(typeof this.options.value != 'undefined'){
				this.model.set("value", this.options.value)
			}

			var self = this;
			this.model.on("change:value", _.throttle(function(){
				if(self.isFocused == false){
					self.$editRegion.html(self.model.get("value"));
				}else{
					this.model.set("value", self.$editRegion.html());
				}
			},300),this);



		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			var self = this;

			this.$editRegion = $("<div/>", {"class":"edit-region", contenteditable:true});
			this.$editRegion.on("focusin", function(){
				self.isFocused = true;
			});
			this.$editRegion.on("focusout", function(){
				self.isFocused = false;
			});
			this.$el.append(this.$editRegion);

			this.mediumEditor = new MediumEditor(this.$editRegion,{
				placeholder:this.options.placeholder,
				firstHeader:"h2",
				secondHeader:"h3",
				buttons: ['bold', 'italic', 'quote','anchor','unorderedlist','orderedlist'],
				buttonLabels: 'fontawesome'
				//buttonLabels:{'bold': '<i class="fa fa-bold"></i>', 'link':'<i class="fa fa-link"></i>'}
			});

			if(typeof this.options.bind != 'undefined' && this._selfModel == false){
				this.$editRegion.on('input', _.throttle(function(){
					self.model.set(self.options.bind, self.$editRegion.html());
				},300));
			}
 
			this.value(this.model.get("value"));

			return this;
		},
		value:function(content){
			if(typeof content != 'undefined'){
				this.model.set("value", content);
				this.$editRegion.html(content);
			}

			return this.$editRegion.html();
		}
	})
};

/**
 * @module Slider
 * @requires Component
 */
UI.Components.Slider = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		$track:null,
		$thumbContainer:null,
		$thumb:null,
		min:0,
		max:1,

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui slider",
				classes:"",
				value:"",
				slider:true
			}, options || {})]);

			this.steps = this.options.steps || 0;

			this.model.set("percent", 0);

			this.model.on("change", function(){
				this.setThumbPosition();
			},this);



		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			if(typeof this.options.label != 'undefined'){
				$label = $("<div/>", {text:this.options.label, "class":"ui label"});
				this.$el.append($label);
			}

			this.$track = $("<div/>", {"class":"ui track"});
			this.$el.append(this.$track);

			this.$thumbContainer = $("<div/>", {"class":"ui thumb-container"});
			this.$track.append(this.$thumbContainer);

			this.$thumb = $("<div/>", {"class":"ui thumb"});
			this.$thumbContainer.append(this.$thumb);

			this.$el.on("mousedown", $.proxy(function(e){
				this.startDrag(e);
			},this));

			this.$el.on("mouseup", $.proxy(function(e){
				this.stopDrag(e);
			},this));

			this.applySteps();

			return this;
		},
		startDrag:function(e){
			this.percent(this.getMousePercent(e));
			$(document).bind('selectstart.slider', function(){ return false; });
			$(document).on("mousemove.slider", $.proxy(function(e){
				this.percent(this.getMousePercent(e));
			},this));
			$(document).trigger("mousemove.slider");
			$(document).on("mouseup.slider", $.proxy(function(e){
				this.stopDrag();
			},this));
		},
		getMousePercent:function(e){
			var parentOffset = this.$thumbContainer.offset(); 
			//or $(this).offset(); if you really just want the current element's offset
			var relX = e.pageX - parentOffset.left;
			var relY = e.pageY - parentOffset.top;
			var percent = relX/(this.$thumbContainer.width());
			if(percent > 1){
				percent = 1;
			}
			if(percent < 0){
				percent = 0;
			}
			return percent;
		},
		stopDrag:function(e){
			$(document).off("mousemove.slider");
			$(document).off("mouseup.slider");
			$(document).off("selectstart.slider");
		},
		setThumbPosition:function(){
			this.$thumb.css("left", (this.percent()*100)+"%");
		},
		applySteps:function(){
			this.$track.children(".step").remove();
			if(this.steps != 0){
				this.steppedValues = [];
				var stepAmount = (this.max-this.min)/this.options.steps;

				for(var i = 0; i < this.options.steps; i++){
					console.log(stepAmount)
					this.steppedValues.push(this.min+(stepAmount*i));

					var $step = $("<div/>", {"class":"step"});
					$step.css("width", (100/this.options.steps)+"%");
					$step.css("left", ((100/this.options.steps)*i)+"%");

					this.$track.prepend($step);
				}
				this.steppedValues.push(this.max);
			}

		},
		percent:function(amount){
			if(typeof amount != 'undefined' && !isNaN(amount)){
				if(this.steps != 0){
					this.model.set("percent", this._closest(amount, this.steppedValues) ); 
				}else{
					this.model.set("percent", amount);
				}
			}
			return this.model.get("percent");
		},
		_closest:function(num, arr) {
            var curr = arr[0];
            var diff = Math.abs (num - curr);
            for (var val = 0; val < arr.length; val++) {
                var newdiff = Math.abs (num - arr[val]);
                if (newdiff < diff) {
                    diff = newdiff;
                    curr = arr[val];
                }
            }
            return curr;
        }
	})

};
;

/**
 * @module Text
 * @requires Component
 */
UI.Components.Text = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		$text:null,

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui text",
				classes:"",
				text:"",
				html:true,
			}, options || {})]);

		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			if(this.options.html == true){
				this.$el.html(this.model);
			}
			
			return this;
		},

		setText:function(string){
			this.$el.text(string);
		},

		setHTML:function(html){
			this.options.html = true;
			this.$el.html(html);
		}
	})

};
;

/**
 * @module UI
 * @requires Namespaces
 */




 _.each(UI.Components, function(component, name){

 	if(typeof component.init != 'undefined'){
 		component.init();
 	}
 	
 	UI[name] = component.constructor;
 });


$(document).ready(function(){
	if($.browser.webkit == true){
		$('body').addClass("webkit");
	}
	if($.browser.mozilla == true){
		$('body').addClass("mozilla");
	}
	if($.browser.msie == true){
		$('body').addClass("msie");
	}
});




/* Copyright (c) 2012 Jeremy McPeak http://www.wdonline.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

(function() {

    function init() {

        // filter out unsupported browsers
        if (Element.prototype.addEventListener || !Object.defineProperty) {
            return {
                loadedForBrowser : false
            };
        }

        // create an MS event object and get prototype
        var proto = document.createEventObject().constructor.prototype;

        /**
     * Indicates whether an event propagates up from the target.
     * @returns Boolean
     */
        Object.defineProperty(proto, "bubbles", {
            get: function() {
                // not a complete list of DOM3 events; some of these IE8 doesn't support
                var bubbleEvents = ["select", "scroll", "click", "dblclick",
                    "mousedown", "mousemove", "mouseout", "mouseover", "mouseup", "wheel", "textinput",
                    "keydown", "keypress", "keyup"],
                    type = this.type;

                for (var i = 0, l = bubbleEvents.length; i < l; i++) {
                    if (type === bubbleEvents[i]) {
                        return true;
                    }
                }

                return false;
            }
        });


        /**
     * Indicates whether or not preventDefault() was called on the event.
     * @returns Boolean
     */
        Object.defineProperty(proto, "defaultPrevented", {
            get: function() {
                // if preventDefault() was never called, or returnValue not given a value
                // then returnValue is undefined
                var returnValue = this.returnValue,
                    undef;

                return !(returnValue === undef || returnValue);
            }
        });


        /**
     * Gets the secondary targets of mouseover and mouseout events (toElement and fromElement)
     * @returns EventTarget or {null}
     */
        Object.defineProperty(proto, "relatedTarget", {
            get: function() {
                var type = this.type;

                if (type === "mouseover" || type === "mouseout") {
                    return (type === "mouseover") ? this.fromElement : this.toElement;
                }

                return null;
            }
        });


        /**
     * Gets the target of the event (srcElement)
     * @returns EventTarget
     */
        Object.defineProperty(proto, "target", {
            get: function() { return this.srcElement; }
        });


        /**
     * Cancels the event if it is cancelable. (returnValue)
     * @returns {undefined}
     */
        proto.preventDefault = function() {
            this.returnValue = false;
        };

        /**
     * Prevents further propagation of the current event. (cancelBubble())
     * @returns {undefined}
     */
        proto.stopPropagation = function() {
            this.cancelBubble = true;
        };

        /***************************************
     *
     * Event Listener Setup
     *    Nothing complex here
     *
     ***************************************/

        /**
     * Determines if the provided object implements EventListener
     * @returns boolean
    */
        var implementsEventListener = function(obj) {
            return (typeof obj !== "function" && typeof obj["handleEvent"] === "function");
        };

        var customELKey = "__eventShim__";

        /**
     * Adds an event listener to the DOM object
     * @returns {undefined}
     */
        var addEventListenerFunc = function(type, handler, useCapture) {
            // useCapture isn't used; it's IE!

            var fn = handler;

            if (implementsEventListener(handler)) {

                if (typeof handler[customELKey] !== "function") {
                    handler[customELKey] = function(e) {
                        handler["handleEvent"](e);
                    };
                }

                fn = handler[customELKey];
            }

            this.attachEvent("on" + type, fn);
        };

        /**
     * Removes an event listener to the DOM object
     * @returns {undefined}
     */
        var removeEventListenerFunc = function(type, handler, useCapture) {
            // useCapture isn't used; it's IE!

            var fn = handler;

            if (implementsEventListener(handler)) {
                fn = handler[customELKey];
            }

            this.detachEvent("on" + type, fn);
        };

        // setup the DOM and window objects
        HTMLDocument.prototype.addEventListener = addEventListenerFunc;
        HTMLDocument.prototype.removeEventListener = removeEventListenerFunc;

        Element.prototype.addEventListener = addEventListenerFunc;
        Element.prototype.removeEventListener = removeEventListenerFunc;

        window.addEventListener = addEventListenerFunc;
        window.removeEventListener = removeEventListenerFunc;

        return {
            loadedForBrowser : true
        };
    }

    // check for AMD support
    if (typeof define === "function" && define["amd"]) {
        define(init);
    } else {
        return init();
    }
    
}());







;

})();