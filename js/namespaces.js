/**
 * @module Namespaces
**/

window.UI = {};
UI.Components = {};


UI._UID = 0;
UI.UID = function(){
	UI._UID += 1;
	return UI._UID.toString();
}