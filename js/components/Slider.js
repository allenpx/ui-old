/**
 * @module Slider
 * @requires Component
 */
UI.Components.Slider = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		$track:null,
		$thumbContainer:null,
		$thumb:null,
		min:0,
		max:1,

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui slider",
				classes:"",
				value:"",
				slider:true
			}, options || {})]);

			this.steps = this.options.steps || 0;

			this.model.set("percent", 0);

			this.model.on("change", function(){
				this.setThumbPosition();
			},this);



		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			if(typeof this.options.label != 'undefined'){
				$label = $("<div/>", {text:this.options.label, "class":"ui label"});
				this.$el.append($label);
			}

			this.$track = $("<div/>", {"class":"ui track"});
			this.$el.append(this.$track);

			this.$thumbContainer = $("<div/>", {"class":"ui thumb-container"});
			this.$track.append(this.$thumbContainer);

			this.$thumb = $("<div/>", {"class":"ui thumb"});
			this.$thumbContainer.append(this.$thumb);

			this.$el.on("mousedown", $.proxy(function(e){
				this.startDrag(e);
			},this));

			this.$el.on("mouseup", $.proxy(function(e){
				this.stopDrag(e);
			},this));

			this.applySteps();

			return this;
		},
		startDrag:function(e){
			this.percent(this.getMousePercent(e));
			$(document).bind('selectstart.slider', function(){ return false; });
			$(document).on("mousemove.slider", $.proxy(function(e){
				this.percent(this.getMousePercent(e));
			},this));
			$(document).trigger("mousemove.slider");
			$(document).on("mouseup.slider", $.proxy(function(e){
				this.stopDrag();
			},this));
		},
		getMousePercent:function(e){
			var parentOffset = this.$thumbContainer.offset(); 
			//or $(this).offset(); if you really just want the current element's offset
			var relX = e.pageX - parentOffset.left;
			var relY = e.pageY - parentOffset.top;
			var percent = relX/(this.$thumbContainer.width());
			if(percent > 1){
				percent = 1;
			}
			if(percent < 0){
				percent = 0;
			}
			return percent;
		},
		stopDrag:function(e){
			$(document).off("mousemove.slider");
			$(document).off("mouseup.slider");
			$(document).off("selectstart.slider");
		},
		setThumbPosition:function(){
			this.$thumb.css("left", (this.percent()*100)+"%");
		},
		applySteps:function(){
			this.$track.children(".step").remove();
			if(this.steps != 0){
				this.steppedValues = [];
				var stepAmount = (this.max-this.min)/this.options.steps;

				for(var i = 0; i < this.options.steps; i++){
					console.log(stepAmount)
					this.steppedValues.push(this.min+(stepAmount*i));

					var $step = $("<div/>", {"class":"step"});
					$step.css("width", (100/this.options.steps)+"%");
					$step.css("left", ((100/this.options.steps)*i)+"%");

					this.$track.prepend($step);
				}
				this.steppedValues.push(this.max);
			}

		},
		percent:function(amount){
			if(typeof amount != 'undefined' && !isNaN(amount)){
				if(this.steps != 0){
					this.model.set("percent", this._closest(amount, this.steppedValues) ); 
				}else{
					this.model.set("percent", amount);
				}
			}
			return this.model.get("percent");
		},
		_closest:function(num, arr) {
            var curr = arr[0];
            var diff = Math.abs (num - curr);
            for (var val = 0; val < arr.length; val++) {
                var newdiff = Math.abs (num - arr[val]);
                if (newdiff < diff) {
                    diff = newdiff;
                    curr = arr[val];
                }
            }
            return curr;
        }
	})

};
