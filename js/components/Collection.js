/**
 * @module Collection
 * @requires Component,Button
 */
UI.Collection = UI.Component.extend({
		group:[],
		$itemContainer:null,
		itemCollection:null,
		selectedItem:null,
		selectedModel:null,
		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui collection",
				classes:"",
				align:"center",
				labelProperty:"label",
				value:null,
				items:[]
			}, options || {})]);

			this.model.set("value", null);
			if(this.options.value != null){
				this.model.set("value", this.options.value);
			}

			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
					this.model.set("value", this.options.model.get(this.options.bind));
				});
				if(typeof this.options.value == 'undefined'){
					this.model.set("value", this.options.model.get(this.options.bind));
				}
			}

			this.listenTo(this.model, "change:value", function(e,value){
				this.setSelected();
				if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){

					if(typeof this.options.property != 'undefined'){
						this.options.model.set(this.options.bind, this.value());
					}else{
						this.options.model.set(this.options.bind, this.model.get("value"));
					}
				}
			});

			if(typeof this.options.items.each == 'undefined'){
				this.items = new Backbone.Collection(this.options.items);
			}else{
				this.items = this.options.items;
			}
			this.items.on("all", function(e,value){
				this.renderItems();
			},this);

		},
		beforeRender:function(){
			this.$el.addClass("align-"+this.options.align);

			if(typeof this.options.label != 'undefined'){
				var label = new UI.Label({
					text:this.options.label
				}).render();
				this.$el.append(label.$el);
			}

			this.createItemContainer();

		},
		createItemContainer:function(){ 
			this.$itemContainer = $("<div/>", {"class":"items"});
		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			this.beforeRender();
			
			this.$el.append(this.$itemContainer);

			this.renderItems();

			this.value(this.model.get("value"));

			return this;
		},
		renderItem:function(item){
			var itemObject = _.clone(item.attributes);
			itemObject.model = item;
			itemObject.label = item.get(this.options.labelProperty);

			var button = new UI.Button(itemObject).render();
			button.on("edit", function(e){
				e.options.model.set("label", e.label);
			},this);

			button.on("click", this.itemClick, this);

			return button;
		},
		renderItems:function(){

			this.$itemContainer.empty();
			this.group = [];
			this.selectedItem = null;
			this.items.each(function(item){
				if(typeof this.options.itemClasses != 'undefined'){
					if(item.attributes.classes == 'undefined'){
						item.attributes.classes = this.options.itemClasses;
					}else{
						item.attributes.classes = item.attributes.classes+" "+this.options.itemClasses;
					}
				}
				var button = this.renderItem(item);
				if(typeof button.$el == 'undefined'){
					this.$itemContainer.append(button);
				}else{
					this.$itemContainer.append(button.$el);
				}
				this.group.push(button);

			},this);

			if(this.selectedItem != null && typeof this.selectedItem != 'undefined'){
				var self = this;
				setTimeout(function(){
					if(self.selectedItem.$el.position().top > (self.$itemContainer.scrollTop()+self.$itemContainer.height())){
						self.$itemContainer.scrollTop(self.selectedItem.$el.position().top);
					}
				},100);
			}

			this.setSelected();

		},


		/**
		@inner
		@memberof UI.List
		@method setSelected
		@param value {Bakcbone.Modal}
		*/ 
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}	

			if(this.options.property != null){ //match on the options.property
				if(valueIsModel){
					value = value.get(this.options.property);
				}
				_.each(this.group, function(item){
					if(item.options.model.get(this.options.property) === value){
						item.active(true);
						this.selectedItem = item;
					}else{
						item.active(false);
					}
				},this);

			}else if(typeof value.attributes != 'undefined'){ //just match model equality
				_.each(this.group, function(item){
					if(item.options.model == value){
						item.active(true);
						this.selectedItem = item;
					}else{
						item.active(false);
					}
				},this);
			}

		},
		value:function(newValue){
			if(typeof newValue != 'undefined' && newValue != null){

				if(typeof this.options.property != 'undefined' && typeof newValue.attributes == 'undefined'){
					var findObj = {};
					findObj[this.options.property] = newValue;
					this.model.set("value", this.items.findWhere(findObj).get(this.options.property) );
				}else{
					this.model.set("value", newValue);
				}

			}

			return this.model.get("value");

		}


});

