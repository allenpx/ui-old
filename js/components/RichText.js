/**
 * @module RichText
 * @requires Component,Button,Toolbar,List
 */ 
UI.Components.RichText = {
	$editRegion:null,
	mediumEditor:null,
	isFocused:false,
	init:function(){
		ElementQuery.add({
			"this-min-width":640,
			"class":"large",
			selector:".ui.rich-text"
		});
	},
	constructor: UI.Component.extend({

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui rich-text",
				classes:"",
				id:"",
				placeholder:"",
				bind:"content"
			}, options || {})]);

			this.model.set("value", "");
			if(typeof this.options.value != 'undefined'){
				this.model.set("value", this.options.value)
			}

			var self = this;
			this.model.on("change:value", _.throttle(function(){
				if(self.isFocused == false){
					self.$editRegion.html(self.model.get("value"));
				}else{
					this.model.set("value", self.$editRegion.html());
				}
			},300),this);



		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			var self = this;

			this.$editRegion = $("<div/>", {"class":"edit-region", contenteditable:true});
			this.$editRegion.on("focusin", function(){
				self.isFocused = true;
			});
			this.$editRegion.on("focusout", function(){
				self.isFocused = false;
			});
			this.$el.append(this.$editRegion);

			this.mediumEditor = new MediumEditor(this.$editRegion,{
				placeholder:this.options.placeholder,
				firstHeader:"h2",
				secondHeader:"h3",
				buttons: ['bold', 'italic', 'quote','anchor','unorderedlist','orderedlist'],
				buttonLabels: 'fontawesome'
				//buttonLabels:{'bold': '<i class="fa fa-bold"></i>', 'link':'<i class="fa fa-link"></i>'}
			});

			if(typeof this.options.bind != 'undefined' && this._selfModel == false){
				this.$editRegion.on('input', _.throttle(function(){
					self.model.set(self.options.bind, self.$editRegion.html());
				},300));
			}
 
			this.value(this.model.get("value"));

			return this;
		},
		value:function(content){
			if(typeof content != 'undefined'){
				this.model.set("value", content);
				this.$editRegion.html(content);
			}

			return this.$editRegion.html();
		}
	})
}