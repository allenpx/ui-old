/**
 * @module Label
 * @requires Component
 */
UI.Label = UI.Component.extend({

	$text:null,
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui label",
			classes:"",
			text:"Label",
			style:"span",
			accessoryRight:null
		}, options || {})]);


		if(typeof this.options.model != 'undefined'){
			this.model = this.options.model;
		}else{
			this.model = new Backbone.Model();
		}
		this.listenTo(this.model, "change", function(){
			this.updateText();
		});

	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$text = $("<"+this.options.style+"/>"); 
		this.$el.append(this.$text);
		

		if(this.options.accessoryRight != null){
			this.accessoryRight = $("<div/>", {"class":"accessory-right"});
			this.accessoryRight.append(this.options.accessoryRight);
			this.$el.prepend(this.accessoryRight);
		}

		this.updateText();

		//button.on("click", setSelected, this);
		return this;
	},
	updateText:function(){
		if(typeof this.model != 'undefined' && typeof this.options.bind != 'undefined'){

			if(typeof this.options.value != 'undefined'){
				this.text(this.model.get(this.options.bind).get(this.options.value));
			}else{
				this.text(this.model.get(this.options.bind));
			}
			
		}else{
			this.text(this.options.text);
		}
	},
	text:function(str){
		if(str != null && typeof str != 'undefined'){
			this.options.text = str;
			this.$text.text(this.options.text);
		}
		return this.options.text;
	},
	edit:function(){

	}
});