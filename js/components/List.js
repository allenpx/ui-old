/**
 * @module List
 * @requires Component,Collection
 */
UI.Components.List = {
	init:function(){
		// ElementQuery.add({
		// 	"this-min-width":640,
		// 	class:"horizontal",
		// 	selector:".ui.switch"
		// });
	},  
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui list"
			}, options || {})]);

			//console.log("Switch", this.options.items);

		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.call(this);
			this.$itemContainer.css("-webkit-overflow-scrolling", "touch");
		},
		renderItems:function(){
			UI.Collection.prototype.renderItems.call(this);

		},
		itemClick:function(event){
			if(this.options.property != null){
				this.value(event.model.get(this.options.property));
			}else{
				this.value(event.model);
			}
			this.trigger("select");
		},


	})
};

