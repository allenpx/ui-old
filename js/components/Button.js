/**
 * @module Button
 * @requires Component
 */
UI.Components.Button = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		$text:null,

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui button",
				classes:"",
				label:"",
				value:"",
				button:true
			}, options || {})]);


		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			if(this.options.button == false){
				this.$el.removeClass("button");
			}
			if(typeof this.options.tabindex != 'undefined'){
				this.$el.attr("tabindex", this.options.tabindex);
			}else{
				this.$el.attr("tabindex",0);
			}
			this.$text = $("<span/>");
			this.$text.text(this.options.label);
			this.$el.append(this.$text);
			
			if(typeof this.options.iconLeft != 'undefined'){
				this.$text.before( $("<i/>", {"class":this.options.iconLeft}) );
				this.$el.addClass("icon-left");
			}
			if(typeof this.options.iconRight != 'undefined'){
				this.$text.after( $("<i/>", {"class":this.options.iconRight}) );
				this.$el.addClass("icon-right");
			}
			if(typeof this.options.icon != 'undefined'){
				this.$el.append( $("<i/>",{"class":this.options.icon}) );
			}
			if(typeof this.options.href != 'undefined'){
				this.$el.attr("href", this.options.href);
			}


			if(typeof this.options.title != 'undefined'){
				this.$el.attr("title", this.options.title);
			}

			this.$el.click($.proxy(function(e) {
				this.clickEvent(e);
		    },this));

			this.$el.keypress($.proxy(function(e) {
		        if(e.which == 13) {
		            this.clickEvent(e);
		        }
		    },this));
				
			
			//button.on("click", setSelected, this);
			return this;
		},
		clickEvent:function(e){
			// if(typeof this.options.pushState != 'undefined' && this.options.pushState == true){
			// 	e.preventDefault();
			// 	if(typeof this.options.defaultHref != 'undefined' && this.options.defaultHref != null){
			// 		Backbone.history.navigate( this.options.defaultHref, true );
			// 	}else{
			// 		Backbone.history.navigate( this.options.href, true );
			// 	}
				
			// }
			if(typeof this.options.click != 'undefined'){
				this.options.click();
			}
			this.trigger("click", this.options);
		},
		/**
		@inner
		@memberof UI.Button
		@method active
		@param value {boolean}
		*/
		active:function(bool){
			this.$el.toggleClass("active", bool);
		},
		/**
		@inner
		@memberof UI.Button
		@method highlight
		@param value {boolean}
		*/
		highlight:function(bool){
			this.$el.toggleClass("highlight", bool);
		},
		
		_disabled:false,
		/**
		@inner
		@memberof UI.Button
		@method disabled
		@param value {boolean} Optional
		@returns {Boolean}
		*/
		disabled:function(bool){
			if(bool != undefined){
				this._disabled = bool;
				this.$el.toggleClass("disabled", this._disabled);
			}
			return this._disabled;
		},
		editLabel:function(){
			this.$text.attr("contenteditable", "true");
			this.$text.selectText();
			var self = this;
			this.$text.bind("keypress", function(e){
				if(e.keyCode==13) { //Enter keycode
					e.preventDefault();
					self.finishEditingLabel();
				}
			});
			setTimeout($.proxy(function(){
				
				this.$text.bind("clickoutside", $.proxy(function(){
					this.finishEditingLabel();
				},this));
			},this),100);

		},
		setLabel:function(labelString){
			this.$text.text(labelString);
		},
		setIcon:function(classString){
			this.options.icon = classString;
			this.$el.find("i").attr("class", classString);
		},
		cleanEditingLabel:function(){
			this.$text.off("clickoutside");
			this.$text.off("keypress");
			this.$text.attr("contenteditable", "false");
		},
		cancelEditingLabel:function(){
			this.$text.text(this.options.label);
		},	
		finishEditingLabel:function(){
			if($.trim(this.$text.text()) == "" ){
				this.cancelEditingLabel();
			}else{
				this.options.label = this.$text.text();
				this.trigger("edit", this.options);
			}
			
			this.cleanEditingLabel();
		}
	})

};
