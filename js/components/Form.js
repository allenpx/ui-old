/**
 * @module Form
 * @requires Component,Button,Label,List,Select,Tabs,Switch,Checkbox
 */
UI.Components.Form = {
	init:function(){
		ElementQuery.add({
			"this-min-width":500,
			"class":"large",
			selector:".ui.form"
		});
	},
	constructor: UI.Component.extend({

		_elementCount:0,
		elements:{},

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui form",
				classes:"",
				structure:[]
			}, options || {})]);

		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			this.renderItems(this.options.structure);

			return this;
		},
		renderItems:function(items){

			_.each(items, function(subItems){
				this.$el.append(this.createRow(subItems));
			},this);

		},
		createRow:function(section){
			var returnedEl = null;
			if(typeof section ==  'undefined' || section == null){

			}else{
				if(typeof section.length != 'undefined'){

					returnedEl = $("<div/>", {"class":"ui row"});
					_.each(section, function(child){
						returnedEl.append(this.createRow(child));
					},this);

				}else if(typeof section == "object"){

					returnedEl = this.createCell(section);

				}
			}

			return returnedEl;
		},
		createCell:function(component){
			if(typeof component.model == 'undefined' && typeof this.options.model != 'undefined'){
				component.model = this.options.model;
			}
			if(typeof component.columns == 'undefined'){
				component.columns = "1-1";
			}

			var cellClasses = "";
			if(typeof component.cellClasses != 'undefined'){
				cellClasses = component.cellClasses;
			}
			var columns = "col-"+component.columns;
			var $cell = $("<div/>", {"class":"ui cell "+columns+" "+cellClasses});
			var newComponent = new UI[component.type](component).render();

			if(typeof component.name == 'undefined'){
				component.name = component.type+this._elementCount;
				this._elementCount++;
			}

			this.elements[component.name] = newComponent;

			$cell.append(newComponent.$el);
			return $cell;
		}
	})
}
