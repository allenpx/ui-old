/**
 * @module Calendar
 * @requires Component
 */
UI.Components.Calendar = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui calendar",
				classes:"",
				label:"",
				value:"",
				button:true
			}, options || {})]);


		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			// Array to store month names
			var months = new Array('January',' February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'); 

			// Array to store month days
			var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

			// Array to store week names
			var weekDay = new Array('Su','Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa');

			var date = new Date();

			var day = date.getDate();
			var month = date.getMonth();
			var year = date.getFullYear();
			var days_in_this_month = monthDays[month];
			var first_week_day = new Date(year, month, 1).getDay();

			// Define the variable which holds the calendar table
			  var calendar_html = '';

			  // Create row for displaying current month and year
			 // calendar_html += '<tr><td class="monthHead" colspan="7">' + months[month] + ' ' + year + '</td></tr>';
			  
			  calendar_html += '<div class="header"><div class="previous"><i class="fa fa-chevron-left fa-fw"></i></div><div class="title">'+months[month]+'</div><div class="next"><i class="fa fa-chevron-right fa-fw"></i></div></div>';

			  calendar_html += '<div class="days">';
			  // Create row to display the week days (Monday - Sunday)
			  calendar_html += '<div class="week">';
			  for(week_day= 0; week_day < 7; week_day++) {
			        calendar_html += '<div class="day-display"><div class="date">' + weekDay[week_day] + '</div></div>';
			  }
			  calendar_html += '</div>';


			  // Fill first row of the month with empty cells until the month starts.
			  calendar_html += '<div class="week">';
			  for(week_day = 0; week_day < first_week_day; week_day++) {
			        calendar_html += '<div class="day"> </div>';
			  }

			  // Populate the days in the month
			  week_day = first_week_day;
			  for(day_counter = 1; day_counter <= days_in_this_month; day_counter++) {

			        week_day %= 7;
			        // Check if week ends, if yes move to next row
			        if(week_day == 0){
			        	calendar_html += '</div><div class="week">';
			        }
			        // Show the current day in bold.
			        if(day == day_counter){
			          calendar_html += '<div class="day clickable today"><div class="date">' + day_counter + '</div></div>';
			        }else{
			          calendar_html += '<div class="day clickable"><div class="date">' + day_counter + '</div></div>';
			        }

			        week_day++;
			  }

			  //Add remaining days
			  for(var i = 0; i < (7-week_day); i++){
			  		calendar_html += '<div class="day"><div class="date">' + "" + '</div></div>';
			  }

			  //close week
			  calendar_html += '</div>';
			  
			  // Close the Calendar table
			  calendar_html += '</div>';
			  calendar_html += '';

			  // Display the calendar.
			  this.$el.html(calendar_html);

		

			return this;
		}
	})

};
