/**
 * @module Select
 * @requires Component,Collection
 */
UI.Components.Select = {
	init:function(){
		
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui select",
				property:"cid"
			}, options || {})]);
		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.apply(this);
		},
		createItemContainer:function(){
			this.$itemContainer = $("<select/>", {"class":"items"});
			this.$itemContainer.change($.proxy(function(){
				if(this.options.property != "cid"){ //set value as a primitive
					this.value(this.group[this.$itemContainer.val()].get(this.options.property));
				}else{ //set value as a model
					this.value(this.group[this.$itemContainer.val()]);
				}
			},this));
		},
		renderItem:function(item){
			var itemValue;
			if(this.options.property == "cid"){
				itemValue = item.cid
			}else{
				itemValue = item.get(this.options.property).toString();
			}
			var $option = $("<option/>", {text:item.get("label"), value:itemValue });
			this.group[itemValue] = item;

			return $option;
		},
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}	

			if(valueIsModel){
				this.$itemContainer.val(value.cid);
			}else{
				this.$itemContainer.val(value);
			}

		}


	})
};













