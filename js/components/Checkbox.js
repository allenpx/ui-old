/**
 * @module Checkbox
 * @requires Component
 */
UI.Checkbox = UI.Component.extend({
	options:{
		_classes:"ui checkbox",
		classes:"",
		bind:""
	},
	$checkbox:null,
	initialize:function(options){

		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui checkbox",
			classes:"",
		}, options || {})]);

		this.model.set("value", false);
		if(typeof this.options.value != 'undefined'){
			this.model.set("value", this.options.value);
		}

		if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
			this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
				this.model.set("value", this.options.model.get(this.options.bind));
			});
			if(typeof this.options.value == 'undefined'){
				this.model.set("value", this.options.model.get(this.options.bind));
			}
		}

		this.listenTo(this.model, "change:value", function(e,value){
			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				if(typeof this.options.property != 'undefined'){
					this.options.model.set(this.options.bind, this.value());
				}else{
					this.options.model.set(this.options.bind, this.model.get("value"));
				}
			}
			this.value(this.model.get("value"));
		});

		this.options.checkboxID = "ui-checkboc-"+UI.UID();


	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$checkbox = $("<input/>", {
			type:"checkbox",
			id:this.options.checkboxID
		});
		this.$el.append(this.$checkbox);
		this.$el.attr("tabindex", "0");
		this.$el.keypress($.proxy(function(e) {
	        if(e.which == 13) {
				if(this.model.get("value") == true){
					this.value(false);
				}else{
					this.value(true);
				}
	        }
	    },this));

		this.$label = $("<label/>", {
			"class":"label",
			"for":this.options.checkboxID
		});
		this.$el.append(this.$label);
		if(typeof this.options.label != 'undefined'){
			this.$label.text(this.options.label);
		}else{
			this.$el.addClass("no-label");
		}

		var self = this;
		this.$checkbox.change(function(){
			self.valueChange();
		});

		this.value(this.model.get("value"));

		return this;
	},
	valueChange:function(){
		this.model.set("value", this.$checkbox.prop('checked'));
	},
	value:function(value){
		if(typeof value != 'undefined'){
			this.model.set("value", value);
			this.$checkbox.prop('checked', value);
		}
		return this.model.get("value");
	}
});