/**
 * @module Component
* @requires Namespaces
 */

UI.Component = Backbone.View.extend({
	defaultOptions :{
		_classes:"ui",
		classes:"" 
	},
	tagName:"div",
	_renderedOnce:false,
	_selfModel:false,
	initialize:function(options){
		
		this.options = _.extend({}, this.defaultOptions, options || {}) 


		this.model = new Backbone.Model();

	},

	render:function(){
		this.$el.empty();
		this.$el.addClass(this.options._classes+" "+this.options.classes);
		if(typeof this.options.id != 'undefined'){
			this.$el.attr("id", this.options.id);
		}

		if(typeof this.options.value != 'undefined'){
			this.$el.attr("data-value", this.options.value);
		}
		this._renderedOnce = true;
		return this;
	}
});