/**
 * @module Switch
 * @requires Component,Collection
 */
UI.Components.Switch = {
	init:function(){
		ElementQuery.add({
			"parent-min-width":640,
			"class":"horizontal",
			selector:".ui.switch"
		}); 
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui switch"
			}, options || {})]);

		}, 
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.call(this);
			this.$itemContainer.css("-webkit-overflow-scrolling", "touch");
		},
		renderItems:function(){
			UI.Collection.prototype.renderItems.call(this);

		},
				itemClick:function(event){
			if(this.options.property != null){
				this.value(event.model.get(this.options.property));
			}else{
				this.value(event.model);
			}
			this.trigger("select");
		},


	})
};

