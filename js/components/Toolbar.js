/**
 * @module Toolbar
 * @requires Component,Button
 */ 
UI.Components.Toolbar = {
	init:function(){
		/*
		ElementQuery.add({
			"this-min-width":640,
			class:"large",
			selector:".ui.form"
		});
*/
	},
	constructor: UI.Component.extend({
		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui toolbar",
				classes:"horizontal",
				id:""
			}, options || {})]);
		},
		render:function(){
			UI.Component.prototype.render.apply(this);
			return this;
		},
		add:function(element, options){
			if(typeof options == 'undefined'){options = {}};
			if(typeof options["class"] == 'undefined') { options["class"] = "" };
			var $wrapper = $('<div class="item '+options["class"]+'"></div>');
			if(typeof element.$el != 'undefined'){
				$wrapper.append(element.$el);
			}else{
				$wrapper.append(element);
			}
			this.$el.append($wrapper);
		}
	})
}