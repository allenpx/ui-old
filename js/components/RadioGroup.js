/**
 * @module RadioGroup
 * @requires Component,Collection
 */
UI.Components.RadioGroup = {
	init:function(){
		
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui radio-group",
				property:"cid"
			}, options || {})]);

			this.options.radioID = "ui-radio-group-"+UI.UID();
		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.apply(this);
		},
		createItemContainer:function(){
			this.$itemContainer = $("<div/>", {"class":"items"});
			// this.$itemContainer.change($.proxy(function(){
			// 	if(this.options.property != "cid"){ //set value as a primitive
			// 		this.value(this.group[this.$itemContainer.val()].get(this.options.property));
			// 	}else{ //set value as a model
			// 		this.value(this.group[this.$itemContainer.val()]);
			// 	}
			// },this));
		},
		renderItem:function(item){
			item = item.attributes;
			item.name = this.options.radioID;
			item.model = this.model;

			var $option = new UI.Radio(item).render();
			this.group[item.value] = $option;

			return $option.$el;
		},
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}

			if(typeof this.group[value] != 'undefined'){
				this.group[value].active(true);
			}




			// if(valueIsModel){
			// 	//this.$itemContainer.val(value.cid);
			// }else{
			// 	this.$itemContainer.children()
			// 	//this.$itemContainer.val(value);
			// }

		}


	})
};













