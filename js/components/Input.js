/**
 * @module Input
 * @requires Component
 */
UI.Input = UI.Component.extend({
	
	$input:null,
	_focused:false,
	events:{
		//"click":"clickEvent"
	},
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [options]);
		var defaultOptions = {
			_classes:"ui input",
			classes:"",
			placeholder:"",
			debounceDelay:250,
			inputType:"text"
		};
		if(typeof options == 'undefined'){options = {};}
		_.extend(this.options, defaultOptions, options);

		this.model.set("value", "");

		if(typeof this.options.model != 'undefined'){
			this.options.model.on("change:"+this.options.bind, function(){
				if(!this._focused){
					this.$input.val(this.options.model.get(this.options.bind));
				}
			},this);
		}

	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		if(typeof this.options.field != 'undefined'){
			this.options.inputType = this.options.field;
		}
		if(this.options.inputType == "input"){
			this.options.inputType = "text";
		}

		if(typeof this.options.label != 'undefined'){
			$label = $("<div/>", {text:this.options.label, "class":"ui label"});
			this.$el.append($label);
		}

		if(typeof this.options.iconLeft != 'undefined'){
			this.$el.append( $("<i/>", {"class":this.options.iconLeft}) );
			this.$el.addClass("icon-left"); 
		}
		if(typeof this.options.iconRight != 'undefined'){
			this.$el.append( $("<i/>", {"class":this.options.iconRight}) );
			this.$el.addClass("icon-right");
		}

		if(this.options.inputType == "textarea"){
			this.$input = $("<textarea/>",{

			});
		}else{
			this.$input = $("<input/>",{
				type:this.options.inputType,
				placeholder:this.options.placeholder
			});
		}




		this.$input.on("focusin", $.proxy(function(){
			this._focused = true;
		},this));
		this.$input.on("focusout", $.proxy(function(){
			this._focused = false;
		},this));

		this.$el.append(this.$input);

		var self = this;
		if(this.options.debounce == true){
			this.$input.on("change keyup keydown",_.debounce(function(e){
				self.inputChange();
				if(e.keyCode == 13){
					self.model.trigger("change")
					if(typeof self.options.model != 'undefined'){
						self.options.model.trigger("change:"+self.options.bind);
					}
				}
			},this.options.debounceDelay));
		}else{
			this.$input.on("change keyup keydown",function(e){
				self.inputChange();
				if(e.keyCode == 13){
					self.model.trigger("change")
					if(typeof self.options.model != 'undefined'){
						self.options.model.trigger("change:"+self.options.bind);
					}
				}
			});
		}

		if(typeof this.bind != 'undefined' && typeof this.options.model != 'undefined'){
			this.$input.val(this.options.model.get(this.options.bind));
		}

		if(typeof this.options.value != 'undefined'){
			this.val(this.options.value);
		}

		return this;
	},
	inputChange:function(){
		if(typeof this.options.model != 'undefined'){
			this.options.model.set(this.options.bind, this.$input.val());
		}
	},
	active:function(bool){
		this.$el.toggleClass("active", bool);
	},
	focus:function(){
		this.$input.focus();
	},
	val:function(value, silient){
		if(typeof value != 'undefined'){
			this.$input.val(value)
			if(silient != true && typeof this.options.model != 'undefined'){
				this.options.model.set(this.options.bind, value);
			}
		}

		return this.$input.val();
	}
});