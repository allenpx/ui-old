/**
 * @module Radio
 * @requires Component
 */
UI.Radio = UI.Component.extend({
	options:{
		_classes:"ui radio",
		classes:"",
		bind:""
	},
	$radio:null,
	initialize:function(options){

		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui radio",
			classes:"",
			label:"",
		}, options || {})]);

		if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
			this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
				this.model.set("value", this.options.model.get(this.options.bind));
			});
			if(typeof this.options.value == 'undefined'){
				this.model.set("value", this.options.model.get(this.options.bind));
			}
		}

		this.listenTo(this.model, "change:value", function(e,value){
			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				if(typeof this.options.property != 'undefined'){
					this.options.model.set(this.options.bind, this.value());
				}else{
					this.options.model.set(this.options.bind, this.model.get("value"));
				}
			}
			//this.value(this.model.get("value"));
		});

		this.options.id = "ui-radio-"+UI.UID();
		if(typeof this.options.name == 'undefined'){
			this.options.name = "ui-radio-"+UI.UID();
		}

		// if(this.options.value == 'undefined'){
		// 	this.options.value = this.options.id;
		// }


	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$radio = $("<input/>", {
			type:"radio",
			id:this.options.id,
			name:this.options.name,
			value:this.options.value.toString()
		});
		this.$el.append(this.$radio);
		this.$el.attr("tabindex", "0");
		this.$el.keypress($.proxy(function(e) {
	        if(e.which == 13) {
				this.active(true);
	        }
	    },this));

	    this.$el.click($.proxy(function(){
	    	this.active(true);
	    },this) );

		if(typeof this.options.label != 'undefined'){
			this.$label = $("<label/>", {
				"class":"label",
				"for":this.options.id
			});
			this.$label.text(this.options.label);
			this.$el.append(this.$label);
		}

		var self = this;
		this.$radio.change(function(){
			self.valueChange();
		});
		
		//this.value(this.model.get("value"));

		return this;
	},
	valueChange:function(){
		this.model.set("value", this.$radio.prop('checked'));
	},
	active:function(value){
		if(typeof value != 'undefined'){
			if(value == true){
				if(this.options.model != 'undefined'){
					this.options.model.set("value", this.options.value);
				}
				this.$radio.prop('checked', value);
			}
		}
		return this.$radio.prop('checked');
	}
});