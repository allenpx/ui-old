/**
 * @module Tabs
 * @requires Component,Button
 */
UI.Tabs = UI.Component.extend({

	tabs:{},
	tabsCollection:null,
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui tabs",
			classes:"",
			bind:"",
		}, options || {})]);
		
		this.model.set("selected", "0");

		this.model.on("change", function(e,value){
			this.setSelected(e.get("selected"));
		},this);

		if(typeof this.options.tabs.each == 'undefined'){
			this.tabsCollection = new Backbone.Collection(options.tabs);
			this.tabsCollection.on("change", function(e,value){
				this.renderTabs();
			},this);
		}else{
			this.tabsCollection = this.options.items;
		}


	},	
	render:function(){
		UI.Component.prototype.render.apply(this);

		if(typeof this.options.label != 'undefined'){
			var label = new UI.Label({
				text:this.options.label
			}).render();
			this.$el.append(label.$el);
		}

		this.$tabsButtonsContainer = $("<div/>", {"class":"tab-buttons"});
		this.$el.append(this.$tabsButtonsContainer);

		this.$tabsContentContainer = $("<div/>", {"class":"tab-content"});
		this.$el.append(this.$tabsContentContainer);

		this.renderTabs();

		return this;
	},
	renderTabs:function(){

		this.tabs = {};
		this.$tabsButtonsContainer.empty();
		this.$tabsContentContainer.empty();
		var firstTab = null;

		this.tabsCollection.each(function(item){
			if(typeof item.attributes != 'undefined'){
				item = item.attributes;
			}

			item.value = "tab_"+UI.UID();
			if(firstTab == null){
				firstTab = item.value;
			}

			var button = new UI.Button({
				label:item.label,
				value:item.value
			}).render();
			button.on("click", this.itemClick, this);


			if(item.value == this.model.get(this.options.bind)){
				button.active(true);
				selectedItem = button;
			}


			var $tab = $("<div/>", {"data-value":item.value});
			if(typeof item.content != 'undefined'){
				$tab.append(item.content);					
			}
			

			this.$tabsButtonsContainer.append(button.$el);
			this.$tabsContentContainer.append($tab);

			this.tabs[item.value] = {
				button:button,
				content:$tab
			}


		},this);

		this.setSelected(firstTab);
		


	},

	itemClick:function(event){
		this.model.set("selected", event.value);
		this.trigger("select", event);
	},
	setSelected:function(value){
		this.$('.tab-content>div.active, .tab-buttons>div.active').toggleClass("active", false);
		this.$('.tab-content>div[data-value="'+value+'"], .tab-buttons>div[data-value="'+value+'"]').toggleClass("active", true);
	}

});

