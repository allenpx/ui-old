module.exports = function(grunt) {

    grunt.initConfig({

        projectName:'ui', //Change this to your project name. Keep in mind it will be the name of the generated .js and .css. Dashed names would be nice.
        
        concat_deps: {

            development: {
                options: {
                    intro: 'js/_intro.js',
                    outro: 'js/_outro.js',
                    out:'temp/ui.js',
                    joinString: ';\n\n'
                },
                files:   {
                    src:  'js/**/*.js'
                }
            },

        },

        handlebars: {
            compile: {
                options: {
                    namespace: "Templates",
                    processName: function(filename) {
                        return (filename.split('templates/')[1]).split('.hbs')[0];
                    }
                },
                files: {
                    "temp/templates.js": ["js/templates/*.hbs"]
                }   
            }
        },

        concat: {
            options: {
              separator: '\n\n',
            },
            js:{
                src: [
                    'temp/templates.js',
                    'node_modules/medium-editor/dist/js/medium-editor.js',
                    'temp/ui.js',
                ],
                dest: 'ui.dev.js',
            },
            less:{
                src: [
                    'node_modules/medium-editor/dist/css/medium-editor.css',
                    //'node_modules/medium-editor/dist/css/themes/default.css',
                    'less/components/*'
                ],
                dest: 'temp/components.less',
            }
        },

        uglify: {
            options: {
                mangle: false,
                compress:false
            },
            app: {
                files: {
                    'ui.js': ['ui.dev.js']
                }
            }
        },

        less: {
            development: {
                options: {
                    paths: ["less"],
                    compress: false,
                    sourceMap:true
                },
                files: {
                    "ui.dev.css": "less/build.less"
                }
            },
            production: {
                options: {
                    paths: ["less"],
                    compress: true,
                    sourceMap:false
                },
                files: {
                    "ui.css": "less/build.less"
                }
            }
        },

        watch:{
            html: {
                files: ['*.html'],
                options:{
                    livereload:{
                        port:6452
                    }
                }
            },
            css: {
                files: ['less/**/*.css', 'less/**/*.less'],
                tasks: ['concat','less'],
                options:{
                    livereload:{
                        port:6452
                    }
                }
            },
            templates: {
                files: ['templates/*.hbs'],
                tasks: ['handlebars','concat_deps','concat'],
                options: {
                  nospawn: true,
                  interrupt: true,
                  livereload:{
                        port:6452
                    }
                },
            },
            js: {
                files: ['js/**/*.js'],
                tasks: ['concat_deps', 'concat', 'uglify'],
                options:{
                    livereload:{
                        port:6452
                    }
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-concat-deps');

    grunt.registerTask('default', [
        'handlebars',
        'concat_deps', 
        'concat',
        'less',
        'uglify',
        'watch'
    ]);

};