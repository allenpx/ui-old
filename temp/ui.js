(function(){


;

/**
 * @module Namespaces
**/

window.UI = {};
UI.Components = {};


UI._UID = 0;
UI.UID = function(){
	UI._UID += 1;
	return UI._UID.toString();
};

/**
 * @module Component
* @requires Namespaces
 */

UI.Component = Backbone.View.extend({
	defaultOptions :{
		_classes:"ui",
		classes:"" 
	},
	tagName:"div",
	_renderedOnce:false,
	_selfModel:false,
	initialize:function(options){
		
		this.options = _.extend({}, this.defaultOptions, options || {}) 


		this.model = new Backbone.Model();

	},

	render:function(){
		this.$el.empty();
		this.$el.addClass(this.options._classes+" "+this.options.classes);
		if(typeof this.options.id != 'undefined'){
			this.$el.attr("id", this.options.id);
		}

		if(typeof this.options.value != 'undefined'){
			this.$el.attr("data-value", this.options.value);
		}
		this._renderedOnce = true;
		return this;
	}
});;

/**
 * @module Button
 * @requires Component
 */
UI.Components.Button = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		$text:null,

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui button",
				classes:"",
				label:"",
				value:"",
				button:true
			}, options || {})]);


		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			if(this.options.button == false){
				this.$el.removeClass("button");
			}
			if(typeof this.options.tabindex != 'undefined'){
				this.$el.attr("tabindex", this.options.tabindex);
			}else{
				this.$el.attr("tabindex",0);
			}
			this.$text = $("<span/>");
			this.$text.text(this.options.label);
			this.$el.append(this.$text);
			
			if(typeof this.options.iconLeft != 'undefined'){
				this.$text.before( $("<i/>", {"class":this.options.iconLeft}) );
				this.$el.addClass("icon-left");
			}
			if(typeof this.options.iconRight != 'undefined'){
				this.$text.after( $("<i/>", {"class":this.options.iconRight}) );
				this.$el.addClass("icon-right");
			}
			if(typeof this.options.icon != 'undefined'){
				this.$el.append( $("<i/>",{"class":this.options.icon}) );
			}
			if(typeof this.options.href != 'undefined'){
				this.$el.attr("href", this.options.href);
			}


			if(typeof this.options.title != 'undefined'){
				this.$el.attr("title", this.options.title);
			}

			this.$el.click($.proxy(function(e) {
				this.clickEvent(e);
		    },this));

			this.$el.keypress($.proxy(function(e) {
		        if(e.which == 13) {
		            this.clickEvent(e);
		        }
		    },this));
				
			
			//button.on("click", setSelected, this);
			return this;
		},
		clickEvent:function(e){
			// if(typeof this.options.pushState != 'undefined' && this.options.pushState == true){
			// 	e.preventDefault();
			// 	if(typeof this.options.defaultHref != 'undefined' && this.options.defaultHref != null){
			// 		Backbone.history.navigate( this.options.defaultHref, true );
			// 	}else{
			// 		Backbone.history.navigate( this.options.href, true );
			// 	}
				
			// }
			if(typeof this.options.click != 'undefined'){
				this.options.click();
			}
			this.trigger("click", this.options);
		},
		/**
		@inner
		@memberof UI.Button
		@method active
		@param value {boolean}
		*/
		active:function(bool){
			this.$el.toggleClass("active", bool);
		},
		/**
		@inner
		@memberof UI.Button
		@method highlight
		@param value {boolean}
		*/
		highlight:function(bool){
			this.$el.toggleClass("highlight", bool);
		},
		
		_disabled:false,
		/**
		@inner
		@memberof UI.Button
		@method disabled
		@param value {boolean} Optional
		@returns {Boolean}
		*/
		disabled:function(bool){
			if(bool != undefined){
				this._disabled = bool;
				this.$el.toggleClass("disabled", this._disabled);
			}
			return this._disabled;
		},
		editLabel:function(){
			this.$text.attr("contenteditable", "true");
			this.$text.selectText();
			var self = this;
			this.$text.bind("keypress", function(e){
				if(e.keyCode==13) { //Enter keycode
					e.preventDefault();
					self.finishEditingLabel();
				}
			});
			setTimeout($.proxy(function(){
				
				this.$text.bind("clickoutside", $.proxy(function(){
					this.finishEditingLabel();
				},this));
			},this),100);

		},
		setLabel:function(labelString){
			this.$text.text(labelString);
		},
		setIcon:function(classString){
			this.options.icon = classString;
			this.$el.find("i").attr("class", classString);
		},
		cleanEditingLabel:function(){
			this.$text.off("clickoutside");
			this.$text.off("keypress");
			this.$text.attr("contenteditable", "false");
		},
		cancelEditingLabel:function(){
			this.$text.text(this.options.label);
		},	
		finishEditingLabel:function(){
			if($.trim(this.$text.text()) == "" ){
				this.cancelEditingLabel();
			}else{
				this.options.label = this.$text.text();
				this.trigger("edit", this.options);
			}
			
			this.cleanEditingLabel();
		}
	})

};
;

/**
 * @module Calendar
 * @requires Component
 */
UI.Components.Calendar = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui calendar",
				classes:"",
				label:"",
				value:"",
				button:true
			}, options || {})]);


		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			// Array to store month names
			var months = new Array('January',' February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'); 

			// Array to store month days
			var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

			// Array to store week names
			var weekDay = new Array('Su','Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa');

			var date = new Date();

			var day = date.getDate();
			var month = date.getMonth();
			var year = date.getFullYear();
			var days_in_this_month = monthDays[month];
			var first_week_day = new Date(year, month, 1).getDay();

			// Define the variable which holds the calendar table
			  var calendar_html = '';

			  // Create row for displaying current month and year
			 // calendar_html += '<tr><td class="monthHead" colspan="7">' + months[month] + ' ' + year + '</td></tr>';
			  
			  calendar_html += '<div class="header"><div class="previous"><i class="fa fa-chevron-left fa-fw"></i></div><div class="title">'+months[month]+'</div><div class="next"><i class="fa fa-chevron-right fa-fw"></i></div></div>';

			  calendar_html += '<div class="days">';
			  // Create row to display the week days (Monday - Sunday)
			  calendar_html += '<div class="week">';
			  for(week_day= 0; week_day < 7; week_day++) {
			        calendar_html += '<div class="day-display"><div class="date">' + weekDay[week_day] + '</div></div>';
			  }
			  calendar_html += '</div>';


			  // Fill first row of the month with empty cells until the month starts.
			  calendar_html += '<div class="week">';
			  for(week_day = 0; week_day < first_week_day; week_day++) {
			        calendar_html += '<div class="day"> </div>';
			  }

			  // Populate the days in the month
			  week_day = first_week_day;
			  for(day_counter = 1; day_counter <= days_in_this_month; day_counter++) {

			        week_day %= 7;
			        // Check if week ends, if yes move to next row
			        if(week_day == 0){
			        	calendar_html += '</div><div class="week">';
			        }
			        // Show the current day in bold.
			        if(day == day_counter){
			          calendar_html += '<div class="day clickable today"><div class="date">' + day_counter + '</div></div>';
			        }else{
			          calendar_html += '<div class="day clickable"><div class="date">' + day_counter + '</div></div>';
			        }

			        week_day++;
			  }

			  //Add remaining days
			  for(var i = 0; i < (7-week_day); i++){
			  		calendar_html += '<div class="day"><div class="date">' + "" + '</div></div>';
			  }

			  //close week
			  calendar_html += '</div>';
			  
			  // Close the Calendar table
			  calendar_html += '</div>';
			  calendar_html += '';

			  // Display the calendar.
			  this.$el.html(calendar_html);

		

			return this;
		}
	})

};
;

/**
 * @module Checkbox
 * @requires Component
 */
UI.Checkbox = UI.Component.extend({
	options:{
		_classes:"ui checkbox",
		classes:"",
		bind:""
	},
	$checkbox:null,
	initialize:function(options){

		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui checkbox",
			classes:"",
		}, options || {})]);

		this.model.set("value", false);
		if(typeof this.options.value != 'undefined'){
			this.model.set("value", this.options.value);
		}

		if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
			this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
				this.model.set("value", this.options.model.get(this.options.bind));
			});
			if(typeof this.options.value == 'undefined'){
				this.model.set("value", this.options.model.get(this.options.bind));
			}
		}

		this.listenTo(this.model, "change:value", function(e,value){
			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				if(typeof this.options.property != 'undefined'){
					this.options.model.set(this.options.bind, this.value());
				}else{
					this.options.model.set(this.options.bind, this.model.get("value"));
				}
			}
			this.value(this.model.get("value"));
		});

		this.options.checkboxID = "ui-checkboc-"+UI.UID();


	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$checkbox = $("<input/>", {
			type:"checkbox",
			id:this.options.checkboxID
		});
		this.$el.append(this.$checkbox);
		this.$el.attr("tabindex", "0");
		this.$el.keypress($.proxy(function(e) {
	        if(e.which == 13) {
				if(this.model.get("value") == true){
					this.value(false);
				}else{
					this.value(true);
				}
	        }
	    },this));

		this.$label = $("<label/>", {
			"class":"label",
			"for":this.options.checkboxID
		});
		this.$el.append(this.$label);
		if(typeof this.options.label != 'undefined'){
			this.$label.text(this.options.label);
		}else{
			this.$el.addClass("no-label");
		}

		var self = this;
		this.$checkbox.change(function(){
			self.valueChange();
		});

		this.value(this.model.get("value"));

		return this;
	},
	valueChange:function(){
		this.model.set("value", this.$checkbox.prop('checked'));
	},
	value:function(value){
		if(typeof value != 'undefined'){
			this.model.set("value", value);
			this.$checkbox.prop('checked', value);
		}
		return this.model.get("value");
	}
});;

/**
 * @module Collection
 * @requires Component,Button
 */
UI.Collection = UI.Component.extend({
		group:[],
		$itemContainer:null,
		itemCollection:null,
		selectedItem:null,
		selectedModel:null,
		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui collection",
				classes:"",
				align:"center",
				labelProperty:"label",
				value:null,
				items:[]
			}, options || {})]);

			this.model.set("value", null);
			if(this.options.value != null){
				this.model.set("value", this.options.value);
			}

			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
					this.model.set("value", this.options.model.get(this.options.bind));
				});
				if(typeof this.options.value == 'undefined'){
					this.model.set("value", this.options.model.get(this.options.bind));
				}
			}

			this.listenTo(this.model, "change:value", function(e,value){
				this.setSelected();
				if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){

					if(typeof this.options.property != 'undefined'){
						this.options.model.set(this.options.bind, this.value());
					}else{
						this.options.model.set(this.options.bind, this.model.get("value"));
					}
				}
			});

			if(typeof this.options.items.each == 'undefined'){
				this.items = new Backbone.Collection(this.options.items);
			}else{
				this.items = this.options.items;
			}
			this.items.on("all", function(e,value){
				this.renderItems();
			},this);

		},
		beforeRender:function(){
			this.$el.addClass("align-"+this.options.align);

			if(typeof this.options.label != 'undefined'){
				var label = new UI.Label({
					text:this.options.label
				}).render();
				this.$el.append(label.$el);
			}

			this.createItemContainer();

		},
		createItemContainer:function(){ 
			this.$itemContainer = $("<div/>", {"class":"items"});
		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			this.beforeRender();
			
			this.$el.append(this.$itemContainer);

			this.renderItems();

			this.value(this.model.get("value"));

			return this;
		},
		renderItem:function(item){
			var itemObject = _.clone(item.attributes);
			itemObject.model = item;
			itemObject.label = item.get(this.options.labelProperty);

			var button = new UI.Button(itemObject).render();
			button.on("edit", function(e){
				e.options.model.set("label", e.label);
			},this);

			button.on("click", this.itemClick, this);

			return button;
		},
		renderItems:function(){

			this.$itemContainer.empty();
			this.group = [];
			this.selectedItem = null;
			this.items.each(function(item){
				if(typeof this.options.itemClasses != 'undefined'){
					if(item.attributes.classes == 'undefined'){
						item.attributes.classes = this.options.itemClasses;
					}else{
						item.attributes.classes = item.attributes.classes+" "+this.options.itemClasses;
					}
				}
				var button = this.renderItem(item);
				if(typeof button.$el == 'undefined'){
					this.$itemContainer.append(button);
				}else{
					this.$itemContainer.append(button.$el);
				}
				this.group.push(button);

			},this);

			if(this.selectedItem != null && typeof this.selectedItem != 'undefined'){
				var self = this;
				setTimeout(function(){
					if(self.selectedItem.$el.position().top > (self.$itemContainer.scrollTop()+self.$itemContainer.height())){
						self.$itemContainer.scrollTop(self.selectedItem.$el.position().top);
					}
				},100);
			}

			this.setSelected();

		},


		/**
		@inner
		@memberof UI.List
		@method setSelected
		@param value {Bakcbone.Modal}
		*/ 
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}	

			if(this.options.property != null){ //match on the options.property
				if(valueIsModel){
					value = value.get(this.options.property);
				}
				_.each(this.group, function(item){
					if(item.options.model.get(this.options.property) === value){
						item.active(true);
						this.selectedItem = item;
					}else{
						item.active(false);
					}
				},this);

			}else if(typeof value.attributes != 'undefined'){ //just match model equality
				_.each(this.group, function(item){
					if(item.options.model == value){
						item.active(true);
						this.selectedItem = item;
					}else{
						item.active(false);
					}
				},this);
			}

		},
		value:function(newValue){
			if(typeof newValue != 'undefined' && newValue != null){

				if(typeof this.options.property != 'undefined' && typeof newValue.attributes == 'undefined'){
					var findObj = {};
					findObj[this.options.property] = newValue;
					this.model.set("value", this.items.findWhere(findObj).get(this.options.property) );
				}else{
					this.model.set("value", newValue);
				}

			}

			return this.model.get("value");

		}


});

;

/**
 * @module Label
 * @requires Component
 */
UI.Label = UI.Component.extend({

	$text:null,
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui label",
			classes:"",
			text:"Label",
			style:"span",
			accessoryRight:null
		}, options || {})]);


		if(typeof this.options.model != 'undefined'){
			this.model = this.options.model;
		}else{
			this.model = new Backbone.Model();
		}
		this.listenTo(this.model, "change", function(){
			this.updateText();
		});

	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$text = $("<"+this.options.style+"/>"); 
		this.$el.append(this.$text);
		

		if(this.options.accessoryRight != null){
			this.accessoryRight = $("<div/>", {"class":"accessory-right"});
			this.accessoryRight.append(this.options.accessoryRight);
			this.$el.prepend(this.accessoryRight);
		}

		this.updateText();

		//button.on("click", setSelected, this);
		return this;
	},
	updateText:function(){
		if(typeof this.model != 'undefined' && typeof this.options.bind != 'undefined'){

			if(typeof this.options.value != 'undefined'){
				this.text(this.model.get(this.options.bind).get(this.options.value));
			}else{
				this.text(this.model.get(this.options.bind));
			}
			
		}else{
			this.text(this.options.text);
		}
	},
	text:function(str){
		if(str != null && typeof str != 'undefined'){
			this.options.text = str;
			this.$text.text(this.options.text);
		}
		return this.options.text;
	},
	edit:function(){

	}
});;

/**
 * @module List
 * @requires Component,Collection
 */
UI.Components.List = {
	init:function(){
		// ElementQuery.add({
		// 	"this-min-width":640,
		// 	class:"horizontal",
		// 	selector:".ui.switch"
		// });
	},  
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui list"
			}, options || {})]);

			//console.log("Switch", this.options.items);

		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.call(this);
			this.$itemContainer.css("-webkit-overflow-scrolling", "touch");
		},
		renderItems:function(){
			UI.Collection.prototype.renderItems.call(this);

		},
		itemClick:function(event){
			if(this.options.property != null){
				this.value(event.model.get(this.options.property));
			}else{
				this.value(event.model);
			}
			this.trigger("select");
		},


	})
};

;

/**
 * @module Select
 * @requires Component,Collection
 */
UI.Components.Select = {
	init:function(){
		
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui select",
				property:"cid"
			}, options || {})]);
		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.apply(this);
		},
		createItemContainer:function(){
			this.$itemContainer = $("<select/>", {"class":"items"});
			this.$itemContainer.change($.proxy(function(){
				if(this.options.property != "cid"){ //set value as a primitive
					this.value(this.group[this.$itemContainer.val()].get(this.options.property));
				}else{ //set value as a model
					this.value(this.group[this.$itemContainer.val()]);
				}
			},this));
		},
		renderItem:function(item){
			var itemValue;
			if(this.options.property == "cid"){
				itemValue = item.cid
			}else{
				itemValue = item.get(this.options.property).toString();
			}
			var $option = $("<option/>", {text:item.get("label"), value:itemValue });
			this.group[itemValue] = item;

			return $option;
		},
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}	

			if(valueIsModel){
				this.$itemContainer.val(value.cid);
			}else{
				this.$itemContainer.val(value);
			}

		}


	})
};













;

/**
 * @module Tabs
 * @requires Component,Button
 */
UI.Tabs = UI.Component.extend({

	tabs:{},
	tabsCollection:null,
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui tabs",
			classes:"",
			bind:"",
		}, options || {})]);
		
		this.model.set("selected", "0");

		this.model.on("change", function(e,value){
			this.setSelected(e.get("selected"));
		},this);

		if(typeof this.options.tabs.each == 'undefined'){
			this.tabsCollection = new Backbone.Collection(options.tabs);
			this.tabsCollection.on("change", function(e,value){
				this.renderTabs();
			},this);
		}else{
			this.tabsCollection = this.options.items;
		}


	},	
	render:function(){
		UI.Component.prototype.render.apply(this);

		if(typeof this.options.label != 'undefined'){
			var label = new UI.Label({
				text:this.options.label
			}).render();
			this.$el.append(label.$el);
		}

		this.$tabsButtonsContainer = $("<div/>", {"class":"tab-buttons"});
		this.$el.append(this.$tabsButtonsContainer);

		this.$tabsContentContainer = $("<div/>", {"class":"tab-content"});
		this.$el.append(this.$tabsContentContainer);

		this.renderTabs();

		return this;
	},
	renderTabs:function(){

		this.tabs = {};
		this.$tabsButtonsContainer.empty();
		this.$tabsContentContainer.empty();
		var firstTab = null;

		this.tabsCollection.each(function(item){
			if(typeof item.attributes != 'undefined'){
				item = item.attributes;
			}

			item.value = "tab_"+UI.UID();
			if(firstTab == null){
				firstTab = item.value;
			}

			var button = new UI.Button({
				label:item.label,
				value:item.value
			}).render();
			button.on("click", this.itemClick, this);


			if(item.value == this.model.get(this.options.bind)){
				button.active(true);
				selectedItem = button;
			}


			var $tab = $("<div/>", {"data-value":item.value});
			if(typeof item.content != 'undefined'){
				$tab.append(item.content);					
			}
			

			this.$tabsButtonsContainer.append(button.$el);
			this.$tabsContentContainer.append($tab);

			this.tabs[item.value] = {
				button:button,
				content:$tab
			}


		},this);

		this.setSelected(firstTab);
		


	},

	itemClick:function(event){
		this.model.set("selected", event.value);
		this.trigger("select", event);
	},
	setSelected:function(value){
		this.$('.tab-content>div.active, .tab-buttons>div.active').toggleClass("active", false);
		this.$('.tab-content>div[data-value="'+value+'"], .tab-buttons>div[data-value="'+value+'"]').toggleClass("active", true);
	}

});

;

/**
 * @module Switch
 * @requires Component,Collection
 */
UI.Components.Switch = {
	init:function(){
		ElementQuery.add({
			"parent-min-width":640,
			"class":"horizontal",
			selector:".ui.switch"
		}); 
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui switch"
			}, options || {})]);

		}, 
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.call(this);
			this.$itemContainer.css("-webkit-overflow-scrolling", "touch");
		},
		renderItems:function(){
			UI.Collection.prototype.renderItems.call(this);

		},
				itemClick:function(event){
			if(this.options.property != null){
				this.value(event.model.get(this.options.property));
			}else{
				this.value(event.model);
			}
			this.trigger("select");
		},


	})
};

;

/**
 * @module Form
 * @requires Component,Button,Label,List,Select,Tabs,Switch,Checkbox
 */
UI.Components.Form = {
	init:function(){
		ElementQuery.add({
			"this-min-width":500,
			"class":"large",
			selector:".ui.form"
		});
	},
	constructor: UI.Component.extend({

		_elementCount:0,
		elements:{},

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui form",
				classes:"",
				structure:[]
			}, options || {})]);

		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			this.renderItems(this.options.structure);

			return this;
		},
		renderItems:function(items){

			_.each(items, function(subItems){
				this.$el.append(this.createRow(subItems));
			},this);

		},
		createRow:function(section){
			var returnedEl = null;
			if(typeof section ==  'undefined' || section == null){

			}else{
				if(typeof section.length != 'undefined'){

					returnedEl = $("<div/>", {"class":"ui row"});
					_.each(section, function(child){
						returnedEl.append(this.createRow(child));
					},this);

				}else if(typeof section == "object"){

					returnedEl = this.createCell(section);

				}
			}

			return returnedEl;
		},
		createCell:function(component){
			if(typeof component.model == 'undefined' && typeof this.options.model != 'undefined'){
				component.model = this.options.model;
			}
			if(typeof component.columns == 'undefined'){
				component.columns = "1-1";
			}

			var cellClasses = "";
			if(typeof component.cellClasses != 'undefined'){
				cellClasses = component.cellClasses;
			}
			var columns = "col-"+component.columns;
			var $cell = $("<div/>", {"class":"ui cell "+columns+" "+cellClasses});
			var newComponent = new UI[component.type](component).render();

			if(typeof component.name == 'undefined'){
				component.name = component.type+this._elementCount;
				this._elementCount++;
			}

			this.elements[component.name] = newComponent;

			$cell.append(newComponent.$el);
			return $cell;
		}
	})
}
;

/**
 * @module Input
 * @requires Component
 */
UI.Input = UI.Component.extend({
	
	$input:null,
	_focused:false,
	events:{
		//"click":"clickEvent"
	},
	initialize:function(options){
		UI.Component.prototype.initialize.apply(this, [options]);
		var defaultOptions = {
			_classes:"ui input",
			classes:"",
			placeholder:"",
			debounceDelay:250,
			inputType:"text"
		};
		if(typeof options == 'undefined'){options = {};}
		_.extend(this.options, defaultOptions, options);

		this.model.set("value", "");

		if(typeof this.options.model != 'undefined'){
			this.options.model.on("change:"+this.options.bind, function(){
				if(!this._focused){
					this.$input.val(this.options.model.get(this.options.bind));
				}
			},this);
		}

	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		if(typeof this.options.field != 'undefined'){
			this.options.inputType = this.options.field;
		}
		if(this.options.inputType == "input"){
			this.options.inputType = "text";
		}

		if(typeof this.options.label != 'undefined'){
			$label = $("<div/>", {text:this.options.label, "class":"ui label"});
			this.$el.append($label);
		}

		if(typeof this.options.iconLeft != 'undefined'){
			this.$el.append( $("<i/>", {"class":this.options.iconLeft}) );
			this.$el.addClass("icon-left"); 
		}
		if(typeof this.options.iconRight != 'undefined'){
			this.$el.append( $("<i/>", {"class":this.options.iconRight}) );
			this.$el.addClass("icon-right");
		}

		if(this.options.inputType == "textarea"){
			this.$input = $("<textarea/>",{

			});
		}else{
			this.$input = $("<input/>",{
				type:this.options.inputType,
				placeholder:this.options.placeholder
			});
		}




		this.$input.on("focusin", $.proxy(function(){
			this._focused = true;
		},this));
		this.$input.on("focusout", $.proxy(function(){
			this._focused = false;
		},this));

		this.$el.append(this.$input);

		var self = this;
		if(this.options.debounce == true){
			this.$input.on("change keyup keydown",_.debounce(function(e){
				self.inputChange();
				if(e.keyCode == 13){
					self.model.trigger("change")
					if(typeof self.options.model != 'undefined'){
						self.options.model.trigger("change:"+self.options.bind);
					}
				}
			},this.options.debounceDelay));
		}else{
			this.$input.on("change keyup keydown",function(e){
				self.inputChange();
				if(e.keyCode == 13){
					self.model.trigger("change")
					if(typeof self.options.model != 'undefined'){
						self.options.model.trigger("change:"+self.options.bind);
					}
				}
			});
		}

		if(typeof this.bind != 'undefined' && typeof this.options.model != 'undefined'){
			this.$input.val(this.options.model.get(this.options.bind));
		}

		if(typeof this.options.value != 'undefined'){
			this.val(this.options.value);
		}

		return this;
	},
	inputChange:function(){
		if(typeof this.options.model != 'undefined'){
			this.options.model.set(this.options.bind, this.$input.val());
		}
	},
	active:function(bool){
		this.$el.toggleClass("active", bool);
	},
	focus:function(){
		this.$input.focus();
	},
	val:function(value, silient){
		if(typeof value != 'undefined'){
			this.$input.val(value)
			if(silient != true && typeof this.options.model != 'undefined'){
				this.options.model.set(this.options.bind, value);
			}
		}

		return this.$input.val();
	}
});;

/**
 * @module Radio
 * @requires Component
 */
UI.Radio = UI.Component.extend({
	options:{
		_classes:"ui radio",
		classes:"",
		bind:""
	},
	$radio:null,
	initialize:function(options){

		UI.Component.prototype.initialize.apply(this, [_.extend({
			_classes:"ui radio",
			classes:"",
			label:"",
		}, options || {})]);

		if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
			this.listenTo(this.options.model, "change:"+this.options.bind, function(e,value){
				this.model.set("value", this.options.model.get(this.options.bind));
			});
			if(typeof this.options.value == 'undefined'){
				this.model.set("value", this.options.model.get(this.options.bind));
			}
		}

		this.listenTo(this.model, "change:value", function(e,value){
			if(typeof this.options.model != 'undefined' && typeof this.options.bind != 'undefined'){
				if(typeof this.options.property != 'undefined'){
					this.options.model.set(this.options.bind, this.value());
				}else{
					this.options.model.set(this.options.bind, this.model.get("value"));
				}
			}
			//this.value(this.model.get("value"));
		});

		this.options.id = "ui-radio-"+UI.UID();
		if(typeof this.options.name == 'undefined'){
			this.options.name = "ui-radio-"+UI.UID();
		}

		// if(this.options.value == 'undefined'){
		// 	this.options.value = this.options.id;
		// }


	},
	render:function(){
		UI.Component.prototype.render.apply(this);

		this.$radio = $("<input/>", {
			type:"radio",
			id:this.options.id,
			name:this.options.name,
			value:this.options.value.toString()
		});
		this.$el.append(this.$radio);
		this.$el.attr("tabindex", "0");
		this.$el.keypress($.proxy(function(e) {
	        if(e.which == 13) {
				this.active(true);
	        }
	    },this));

	    this.$el.click($.proxy(function(){
	    	this.active(true);
	    },this) );

		if(typeof this.options.label != 'undefined'){
			this.$label = $("<label/>", {
				"class":"label",
				"for":this.options.id
			});
			this.$label.text(this.options.label);
			this.$el.append(this.$label);
		}

		var self = this;
		this.$radio.change(function(){
			self.valueChange();
		});
		
		//this.value(this.model.get("value"));

		return this;
	},
	valueChange:function(){
		this.model.set("value", this.$radio.prop('checked'));
	},
	active:function(value){
		if(typeof value != 'undefined'){
			if(value == true){
				if(this.options.model != 'undefined'){
					this.options.model.set("value", this.options.value);
				}
				this.$radio.prop('checked', value);
			}
		}
		return this.$radio.prop('checked');
	}
});;

/**
 * @module RadioGroup
 * @requires Component,Collection
 */
UI.Components.RadioGroup = {
	init:function(){
		
	}, 
	constructor: UI.Collection.extend({
		initialize:function(options){
			UI.Collection.prototype.initialize.apply(this, [_.extend({
				_classes:"ui radio-group",
				property:"cid"
			}, options || {})]);

			this.options.radioID = "ui-radio-group-"+UI.UID();
		},
		beforeRender:function(){
			UI.Collection.prototype.beforeRender.apply(this);
		},
		createItemContainer:function(){
			this.$itemContainer = $("<div/>", {"class":"items"});
			// this.$itemContainer.change($.proxy(function(){
			// 	if(this.options.property != "cid"){ //set value as a primitive
			// 		this.value(this.group[this.$itemContainer.val()].get(this.options.property));
			// 	}else{ //set value as a model
			// 		this.value(this.group[this.$itemContainer.val()]);
			// 	}
			// },this));
		},
		renderItem:function(item){
			item = item.attributes;
			item.name = this.options.radioID;
			item.model = this.model;

			var $option = new UI.Radio(item).render();
			this.group[item.value] = $option;

			return $option.$el;
		},
		setSelected:function(){
			var value = this.value();
			var valueIsModel = true;
			if(value == null){return false;}
			if(typeof value.attributes == 'undefined'){
				valueIsModel = false;
			}

			if(typeof this.group[value] != 'undefined'){
				this.group[value].active(true);
			}




			// if(valueIsModel){
			// 	//this.$itemContainer.val(value.cid);
			// }else{
			// 	this.$itemContainer.children()
			// 	//this.$itemContainer.val(value);
			// }

		}


	})
};













;

/**
 * @module Toolbar
 * @requires Component,Button
 */ 
UI.Components.Toolbar = {
	init:function(){
		/*
		ElementQuery.add({
			"this-min-width":640,
			class:"large",
			selector:".ui.form"
		});
*/
	},
	constructor: UI.Component.extend({
		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui toolbar",
				classes:"horizontal",
				id:""
			}, options || {})]);
		},
		render:function(){
			UI.Component.prototype.render.apply(this);
			return this;
		},
		add:function(element, options){
			if(typeof options == 'undefined'){options = {}};
			if(typeof options["class"] == 'undefined') { options["class"] = "" };
			var $wrapper = $('<div class="item '+options["class"]+'"></div>');
			if(typeof element.$el != 'undefined'){
				$wrapper.append(element.$el);
			}else{
				$wrapper.append(element);
			}
			this.$el.append($wrapper);
		}
	})
};

/**
 * @module RichText
 * @requires Component,Button,Toolbar,List
 */ 
UI.Components.RichText = {
	$editRegion:null,
	mediumEditor:null,
	isFocused:false,
	init:function(){
		ElementQuery.add({
			"this-min-width":640,
			"class":"large",
			selector:".ui.rich-text"
		});
	},
	constructor: UI.Component.extend({

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui rich-text",
				classes:"",
				id:"",
				placeholder:"",
				bind:"content"
			}, options || {})]);

			this.model.set("value", "");
			if(typeof this.options.value != 'undefined'){
				this.model.set("value", this.options.value)
			}

			var self = this;
			this.model.on("change:value", _.throttle(function(){
				if(self.isFocused == false){
					self.$editRegion.html(self.model.get("value"));
				}else{
					this.model.set("value", self.$editRegion.html());
				}
			},300),this);



		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			var self = this;

			this.$editRegion = $("<div/>", {"class":"edit-region", contenteditable:true});
			this.$editRegion.on("focusin", function(){
				self.isFocused = true;
			});
			this.$editRegion.on("focusout", function(){
				self.isFocused = false;
			});
			this.$el.append(this.$editRegion);

			this.mediumEditor = new MediumEditor(this.$editRegion,{
				placeholder:this.options.placeholder,
				firstHeader:"h2",
				secondHeader:"h3",
				buttons: ['bold', 'italic', 'quote','anchor','unorderedlist','orderedlist'],
				buttonLabels: 'fontawesome'
				//buttonLabels:{'bold': '<i class="fa fa-bold"></i>', 'link':'<i class="fa fa-link"></i>'}
			});

			if(typeof this.options.bind != 'undefined' && this._selfModel == false){
				this.$editRegion.on('input', _.throttle(function(){
					self.model.set(self.options.bind, self.$editRegion.html());
				},300));
			}
 
			this.value(this.model.get("value"));

			return this;
		},
		value:function(content){
			if(typeof content != 'undefined'){
				this.model.set("value", content);
				this.$editRegion.html(content);
			}

			return this.$editRegion.html();
		}
	})
};

/**
 * @module Slider
 * @requires Component
 */
UI.Components.Slider = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		$track:null,
		$thumbContainer:null,
		$thumb:null,
		min:0,
		max:1,

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui slider",
				classes:"",
				value:"",
				slider:true
			}, options || {})]);

			this.steps = this.options.steps || 0;

			this.model.set("percent", 0);

			this.model.on("change", function(){
				this.setThumbPosition();
			},this);



		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			if(typeof this.options.label != 'undefined'){
				$label = $("<div/>", {text:this.options.label, "class":"ui label"});
				this.$el.append($label);
			}

			this.$track = $("<div/>", {"class":"ui track"});
			this.$el.append(this.$track);

			this.$thumbContainer = $("<div/>", {"class":"ui thumb-container"});
			this.$track.append(this.$thumbContainer);

			this.$thumb = $("<div/>", {"class":"ui thumb"});
			this.$thumbContainer.append(this.$thumb);

			this.$el.on("mousedown", $.proxy(function(e){
				this.startDrag(e);
			},this));

			this.$el.on("mouseup", $.proxy(function(e){
				this.stopDrag(e);
			},this));

			this.applySteps();

			return this;
		},
		startDrag:function(e){
			this.percent(this.getMousePercent(e));
			$(document).bind('selectstart.slider', function(){ return false; });
			$(document).on("mousemove.slider", $.proxy(function(e){
				this.percent(this.getMousePercent(e));
			},this));
			$(document).trigger("mousemove.slider");
			$(document).on("mouseup.slider", $.proxy(function(e){
				this.stopDrag();
			},this));
		},
		getMousePercent:function(e){
			var parentOffset = this.$thumbContainer.offset(); 
			//or $(this).offset(); if you really just want the current element's offset
			var relX = e.pageX - parentOffset.left;
			var relY = e.pageY - parentOffset.top;
			var percent = relX/(this.$thumbContainer.width());
			if(percent > 1){
				percent = 1;
			}
			if(percent < 0){
				percent = 0;
			}
			return percent;
		},
		stopDrag:function(e){
			$(document).off("mousemove.slider");
			$(document).off("mouseup.slider");
			$(document).off("selectstart.slider");
		},
		setThumbPosition:function(){
			this.$thumb.css("left", (this.percent()*100)+"%");
		},
		applySteps:function(){
			this.$track.children(".step").remove();
			if(this.steps != 0){
				this.steppedValues = [];
				var stepAmount = (this.max-this.min)/this.options.steps;

				for(var i = 0; i < this.options.steps; i++){
					console.log(stepAmount)
					this.steppedValues.push(this.min+(stepAmount*i));

					var $step = $("<div/>", {"class":"step"});
					$step.css("width", (100/this.options.steps)+"%");
					$step.css("left", ((100/this.options.steps)*i)+"%");

					this.$track.prepend($step);
				}
				this.steppedValues.push(this.max);
			}

		},
		percent:function(amount){
			if(typeof amount != 'undefined' && !isNaN(amount)){
				if(this.steps != 0){
					this.model.set("percent", this._closest(amount, this.steppedValues) ); 
				}else{
					this.model.set("percent", amount);
				}
			}
			return this.model.get("percent");
		},
		_closest:function(num, arr) {
            var curr = arr[0];
            var diff = Math.abs (num - curr);
            for (var val = 0; val < arr.length; val++) {
                var newdiff = Math.abs (num - arr[val]);
                if (newdiff < diff) {
                    diff = newdiff;
                    curr = arr[val];
                }
            }
            return curr;
        }
	})

};
;

/**
 * @module Text
 * @requires Component
 */
UI.Components.Text = {

	init:function(){
		
	},
	constructor:UI.Component.extend({

		$text:null,

		initialize:function(options){
			UI.Component.prototype.initialize.apply(this, [_.extend({
				_classes:"ui text",
				classes:"",
				text:"",
				html:true,
			}, options || {})]);

		},
		render:function(){
			UI.Component.prototype.render.apply(this);

			if(this.options.html == true){
				this.$el.html(this.model);
			}
			
			return this;
		},

		setText:function(string){
			this.$el.text(string);
		},

		setHTML:function(html){
			this.options.html = true;
			this.$el.html(html);
		}
	})

};
;

/**
 * @module UI
 * @requires Namespaces
 */




 _.each(UI.Components, function(component, name){

 	if(typeof component.init != 'undefined'){
 		component.init();
 	}
 	
 	UI[name] = component.constructor;
 });


$(document).ready(function(){
	if($.browser.webkit == true){
		$('body').addClass("webkit");
	}
	if($.browser.mozilla == true){
		$('body').addClass("mozilla");
	}
	if($.browser.msie == true){
		$('body').addClass("msie");
	}
});




/* Copyright (c) 2012 Jeremy McPeak http://www.wdonline.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

(function() {

    function init() {

        // filter out unsupported browsers
        if (Element.prototype.addEventListener || !Object.defineProperty) {
            return {
                loadedForBrowser : false
            };
        }

        // create an MS event object and get prototype
        var proto = document.createEventObject().constructor.prototype;

        /**
     * Indicates whether an event propagates up from the target.
     * @returns Boolean
     */
        Object.defineProperty(proto, "bubbles", {
            get: function() {
                // not a complete list of DOM3 events; some of these IE8 doesn't support
                var bubbleEvents = ["select", "scroll", "click", "dblclick",
                    "mousedown", "mousemove", "mouseout", "mouseover", "mouseup", "wheel", "textinput",
                    "keydown", "keypress", "keyup"],
                    type = this.type;

                for (var i = 0, l = bubbleEvents.length; i < l; i++) {
                    if (type === bubbleEvents[i]) {
                        return true;
                    }
                }

                return false;
            }
        });


        /**
     * Indicates whether or not preventDefault() was called on the event.
     * @returns Boolean
     */
        Object.defineProperty(proto, "defaultPrevented", {
            get: function() {
                // if preventDefault() was never called, or returnValue not given a value
                // then returnValue is undefined
                var returnValue = this.returnValue,
                    undef;

                return !(returnValue === undef || returnValue);
            }
        });


        /**
     * Gets the secondary targets of mouseover and mouseout events (toElement and fromElement)
     * @returns EventTarget or {null}
     */
        Object.defineProperty(proto, "relatedTarget", {
            get: function() {
                var type = this.type;

                if (type === "mouseover" || type === "mouseout") {
                    return (type === "mouseover") ? this.fromElement : this.toElement;
                }

                return null;
            }
        });


        /**
     * Gets the target of the event (srcElement)
     * @returns EventTarget
     */
        Object.defineProperty(proto, "target", {
            get: function() { return this.srcElement; }
        });


        /**
     * Cancels the event if it is cancelable. (returnValue)
     * @returns {undefined}
     */
        proto.preventDefault = function() {
            this.returnValue = false;
        };

        /**
     * Prevents further propagation of the current event. (cancelBubble())
     * @returns {undefined}
     */
        proto.stopPropagation = function() {
            this.cancelBubble = true;
        };

        /***************************************
     *
     * Event Listener Setup
     *    Nothing complex here
     *
     ***************************************/

        /**
     * Determines if the provided object implements EventListener
     * @returns boolean
    */
        var implementsEventListener = function(obj) {
            return (typeof obj !== "function" && typeof obj["handleEvent"] === "function");
        };

        var customELKey = "__eventShim__";

        /**
     * Adds an event listener to the DOM object
     * @returns {undefined}
     */
        var addEventListenerFunc = function(type, handler, useCapture) {
            // useCapture isn't used; it's IE!

            var fn = handler;

            if (implementsEventListener(handler)) {

                if (typeof handler[customELKey] !== "function") {
                    handler[customELKey] = function(e) {
                        handler["handleEvent"](e);
                    };
                }

                fn = handler[customELKey];
            }

            this.attachEvent("on" + type, fn);
        };

        /**
     * Removes an event listener to the DOM object
     * @returns {undefined}
     */
        var removeEventListenerFunc = function(type, handler, useCapture) {
            // useCapture isn't used; it's IE!

            var fn = handler;

            if (implementsEventListener(handler)) {
                fn = handler[customELKey];
            }

            this.detachEvent("on" + type, fn);
        };

        // setup the DOM and window objects
        HTMLDocument.prototype.addEventListener = addEventListenerFunc;
        HTMLDocument.prototype.removeEventListener = removeEventListenerFunc;

        Element.prototype.addEventListener = addEventListenerFunc;
        Element.prototype.removeEventListener = removeEventListenerFunc;

        window.addEventListener = addEventListenerFunc;
        window.removeEventListener = removeEventListenerFunc;

        return {
            loadedForBrowser : true
        };
    }

    // check for AMD support
    if (typeof define === "function" && define["amd"]) {
        define(init);
    } else {
        return init();
    }
    
}());







;

})();